.checkout
=========

A Symfony project created on November 17, 2016, 4:40 pm.


Zur Installation in einem Terminal wie folgt vorgehen:

    cd /path/to/project
    composer install
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update -f
    
    php bin/console server:run


php bin/console cache:clear --env=prod --no-debug

