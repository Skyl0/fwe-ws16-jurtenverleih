<?php

namespace UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Form\Type\StaffRegistrationType;

class StaffController extends UserController
{

    /**
     * @Post()
     */
    public function registerStaffAction(Request $request)
    {
        $discriminator = $this->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('UserBundle\Entity\Staff');

        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->createUser();
        $form = $this->createForm(StaffRegistrationType::class, $user);
        $form->submit($request->request->all(), false);
        $data = $request->request->all();

        if ($userManager->findUserByEmail($data['email'])) {
            $error = array(
                'message' => 'User with email ' . $data['email'] . ' already exists.'
            );
            $view = $this->view($error, Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        $userManager->updateUser($user, true);
        return $this->generateToken($user, Response::HTTP_CREATED);
    }

    public function deleteStaffAction($staffId)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }
}
