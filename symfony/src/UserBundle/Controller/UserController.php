<?php
namespace UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends FOSRestController
{
    /**
     * @Post()
     */
    public function loginAction(Request $request)
    {
        $data = $request->request->all();
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->findUserByEmail($data['email']);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        if (!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
            $view = $this->view(new AccessDeniedException(), Response::HTTP_UNAUTHORIZED);
            return $this->handleView($view);
        }

        return $this->generateToken($user);
    }

    protected function generateToken($user, $statusCode = 200)
    {
        $token = $this->get('lexik_jwt_authentication.jwt_manager')->create($user);

        $response = array(
            'token' => $token
        );

        $view = $this->view($response, $statusCode);
        return $this->handleView($view);
    }

    public function getUsersAction()
    {
        $userManager = $this->get('pugx_user_manager');
        $users = $userManager->findUsers();

        $view = $this->view($users, Response::HTTP_OK);
        return $this->handleView($view);
    }

    public function getUserAction(Request $request, $userId)
    {
        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->findUserBy(array('id' => $userId));

        $view = $this->view($user, Response::HTTP_OK);
        return $this->handleView($view);
    }

    public function patchUserAction(Request $request, $userId)
    {
        $requestingUser = $this->getUser();
        if (!($requestingUser->getId() == $userId || $this->isGranted('ROLE_ADMIN'))) {
            throw new AccessDeniedHttpException();
        }

        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->findUserBy(['id' => $userId]);

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $userClass = get_class($user);
        $discriminator = $this->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass($userClass);

        $form = $this->get('pugx_multi_user.profile_form_factory')->createForm();
        $form->setData($user);
        $form->submit($request->request->all(), false);

        //$form = $this->createForm($discriminator->getFormName());

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }


        try {
            $userManager->updateUser($user);
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }

        $view = $this->view(null, Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }

    public function deleteUserAction($userId)
    {
        $requestingUser = $this->getUser();
        if (!($requestingUser->getId() == $userId || $this->isGranted('ROLE_ADMIN'))) {
            throw new AccessDeniedException();
        }

        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->findUserBy(['id' => $userId]);
        if (!$user) {
            throw new NotFoundHttpException();
        }

        try {
            $userManager->deleteUser($user);
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }

        $view = $this->view(null, Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}