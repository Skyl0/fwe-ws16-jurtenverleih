<?php

namespace UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Form\Type\CustomerRegistrationType;

class CustomerController extends UserController
{

    /**
     * @Post()
     */
    public function registerCustomerAction(Request $request)
    {
        $discriminator = $this->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('UserBundle\Entity\Customer');

        $userManager = $this->get('pugx_user_manager');
        $user = $userManager->createUser();
        $form = $this->createForm(CustomerRegistrationType::class, $user);
        $form->submit($request->request->all(), false);
        $data = $request->request->all();

        if ($userManager->findUserByEmail($data['email'])) {
            $error = array(
                'message' => 'User with email ' . $data['email'] . ' already exists.'
            );
            $view = $this->view($error, Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        try {
            $userManager->updateUser($user, true);
            return $this->generateToken($user, Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }
}