<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 11.12.2016
 * Time: 17:43
 */

namespace UserBundle\Form\Type;


use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerProfileType extends UserProfileType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('givenname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('company', TextType::class, array(
                'required' => false
            ))
            ->add('street', TextType::class)
            ->add('zipcode', TextType::class)
            ->add('city', TextType::class)
            ->add('country', CountryType::class)
            ->add('email', EmailType::class)
            ->add('phone', NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Customer',
        ));
    }
}