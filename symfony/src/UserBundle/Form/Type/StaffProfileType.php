<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 09.12.2016
 * Time: 18:33
 */

namespace UserBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffProfileType extends UserProfileType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Staff',
        ));
    }
}