<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 13.12.2016
 * Time: 15:06
 */

namespace AppBundle\Form\Type;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryBookingReferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'ProductBundle\Entity\Category',
                'choice_label' => 'name',
                'required' => true
            ))
            ->add('name', HiddenType::class, array(
                'required' => false,
                'mapped' => false
            ))
            ->add('price', HiddenType::class, array(
                'required' => false,
                'mapped' => false
            ))
            ->add('amount', IntegerType::class, array(
                'required' => false,
                'empty_data' => '1'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CategoryBookingReference',
            'csrf_protection' => false
        ));
    }
}