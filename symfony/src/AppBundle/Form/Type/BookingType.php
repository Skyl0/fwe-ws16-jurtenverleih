<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\CategoryBookingReference;
use AppBundle\Form\Type\StateHistoryType as StateHistoryTypeee;
use ProductBundle\Form\Type\ProductType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use UserBundle\Form\Type\CustomerProfileType;
use UserBundle\Form\Type\CustomerRegistrationType;
use ProductBundle\Form\Type\CategoryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 26.11.2016
 * Time: 11:47
 */
class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('comment', TextType::class, array(
            'required'=>false
        ))
        ->add('startDate', DateType::class, array(
            'widget' => 'single_text',
            'required'=>true
        ))
        ->add('dueDate', DateType::class, array(
            'widget' => 'single_text',
            'required'=>true
        ))
        ->add('categorys',  CollectionType::class, array(
            'entry_type' => CategoryBookingReferenceType::class,
            'required' => true,
            'by_reference' => false,
            'allow_add' => true
        ));
}

    public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults(array(
        'data_class'=>'AppBundle\Entity\Booking',
        //'allow_extra_fields' => true,
        'csrf_protection'=>false
    ));
}
}