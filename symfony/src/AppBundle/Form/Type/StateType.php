<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 29.12.2016
 * Time: 19:47
 */

namespace AppBundle\Form\Type;

use AppBundle\DBAL\Prio as PrioType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;



class StateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'=>true
            ))
            ->add('prio', TextType::class, array(
                'required'=>true
            ))
            ->add('default', CheckboxType::class, array(
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>'AppBundle\Entity\State',
            'csrf_protection'=>false
        ));
    }
}