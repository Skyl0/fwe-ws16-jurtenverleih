<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 28.12.2016
 * Time: 21:40
 */

namespace AppBundle\Form\Type;



use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductBookingReferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('booking', EntityType::class, array(
                'class'=>'AppBundle\Entity\Booking',
                'choice_label' => 'id',
                'required'=>true
            ))*/
            ->add('product', EntityType::class, array(
                'class' => 'ProductBundle\Entity\Product',
                'choice_label' => 'name',
                'required'=>true
            ))
            ->add('amount', IntegerType::class, array(
                'required'=>false,
                'empty_data'=>'1'
            ))
            /* Wird automatisch gesetzt
            ->add('price', MoneyType::class, array(
            'required'=>false
            ))*/
            ->add('staff', EntityType::class, array(
                'class' => 'UserBundle\Entity\Staff',
                'required'=>false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>'AppBundle\Entity\ProductBookingReference',
            'csrf_protection'=>false
        ));
    }
}