<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 14.01.2017
 * Time: 20:04
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffBookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextType::class, array(
                'required' => false
            ))
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('dueDate', DateType::class, array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('categorys', CollectionType::class, array(
                'entry_type' => CategoryBookingReferenceType::class,
                'required' => true,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('products', CollectionType::class, array(
                'entry_type' => ProductBookingReferenceType::class,
                'required' => false,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('state', StateHistoryType::class, array(
                'required' => false,
                'by_reference' => false
            ));
//        ->add('comment',TextType::class, array(
//            ''
//        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => 'AppBundle\Entity\Booking'
        ));
    }
}