<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 29.12.2016
 * Time: 17:22
 */

namespace AppBundle\Repository;


class StateRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $customer
     * @return array
     */
    public function orderby($customer)
    {
        $qb = $this->createQueryBuilder('b');
        return $qb
            ->orderBy('b.:customer','ASC')
            ->setParameter(':customer', $customer)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $customer
     * @return array
     */
    public function findBySearch($customer)
    {
        $qb = $this->createQueryBuilder('b');
        return $qb
            ->andWhere(
                $qb->expr()->like('b.name', ':customer')
            )
            ->setParameter(':customer', '%'.$customer.'%')
            ->getQuery()
            ->getResult();
    }
}