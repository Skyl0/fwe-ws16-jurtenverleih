<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 30.01.2017
 * Time: 23:46
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class OnlyDifferentProducts
 * @package AppBundle\Validator\Constraints
 * @Annotation()
 */
class OnlyDifferentProducts extends Constraint
{
    public $message = 'The submitted data has the same product multiple times';
}