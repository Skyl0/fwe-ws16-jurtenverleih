<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 30.01.2017
 * Time: 23:48
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OnlyDifferentProductsValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $products = $value->getValues();
        $productIDs = array();
        foreach ($products as $product) {
            $productIDs[] = $product->getProduct()->getId();
        }
        $uniqueProductIDs = array_unique($productIDs);

        if (count($uniqueProductIDs) < count($productIDs)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}