<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Booking;
use AppBundle\Entity\CategoryBookingReference;
use AppBundle\Entity\StateHistory;
use AppBundle\Form\Type\BookingStaffType;
use AppBundle\Form\Type\BookingType;
use AppBundle\Form\Type\ProductBookingReferenceType;
use AppBundle\Form\Type\StaffBookingType;
use AppBundle\Repository\BookingRepository;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\VarDumper\VarDumper;
use UserBundle\Entity\Customer;
use UserBundle\Repository\CustomerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookingController extends FOSRestController
{


    /**
     * REST action which returns all Bookings
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getBookingsAction(Request $request)
    {
        $headers = $request->headers->all();
        if (array_key_exists('customer',$headers)){
            $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')->findBookingByUser($headers['customer'][0]);
        }
        /*else if(array_key_exists('search',$headers)){
           $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')->findBySearch($headers['search'][0]);
        }*/
        else  {
            $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')->findAll();
        }

        $view = $this->view($booking);
        return $this->handleView($view);
    }

    /**
     * REST action which returns a Booking by id.
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getBookingAction(Request $request, $id) {
        $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')->find($id);
        $this->getDoctrine()->getRepository('AppBundle:Booking')
            ->findOneBy(array('id' => $id));

        if (!$booking) {
            throw new NotFoundHttpException();
        }

        $requestingUser = $this->getUser();
        if (!($requestingUser->getId() == $booking->getCustomer()->getId() || $this->isGranted('ROLE_ADMIN'))) {
            throw new AccessDeniedHttpException();
        }

        $view = $this->view($booking);
        return $this->handleView($view);
    }

    /**
     * REST action which creates a new Booking.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postBookingAction(Request $request)
    {
        if (empty($request)) {
            throw new BadRequestHttpException("Empty Request");
        }


        $booking = new Booking();

        //Validiere Booking
        $form = $this->createForm(BookingType::class, $booking);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        //Todo: state ist am Anfang immer ???? muss irgendwo definiert werden
        //$request->request->set('state',0);

        $requestingUser = $this->getUser();
        $booking->setCustomer($requestingUser);
        $defaultState = $this->getDoctrine()->getRepository('AppBundle:State')->findOneBy(array('default' => true));
        $initialState = new StateHistory();
        $initialState->setUser($requestingUser);
        $initialState->setState($defaultState);
        $booking->setState($initialState);

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $data = array("message" => "Booking successfully created!", "booking" => $booking);
        $view = $this->view($data);
        return $this->handleView($view);
    }



    /**
     * REST action which partially updates an existing Booking.
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patchBookingCustomerAction(Request $request, $id)
    {
        try {
            $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
        if ($booking == null){
            throw new NotFoundHttpException();
        }

        //Validiere Booking
        $form = $this->createForm(BookingType::class, $booking);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        $data = array("message" => "Booking successfully updated!", "booking" => $booking);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
     *
     */
    public function patchBookingStaffAction(Request $request, $bookingId)
    {
        $vd = new VarDumper();
        $vd->dump($request);
        if(!$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $booking = $em->getRepository('AppBundle:Booking')
            ->findOneBy(array('id' => $bookingId));

        if (!$booking) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(StaffBookingType::class, $booking);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        try {
            $booking->calculateTotalPrice();
            $em->persist($booking);
            $em->flush();
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }

    public function getBookingBillAction($bookingId)
    {
        $booking = $this->getDoctrine()->getRepository('AppBundle:Booking')
            ->findOneBy(array('id' => $bookingId));

        if (!$booking) {
            throw new NotFoundHttpException();
        }

        $requestingUser = $this->getUser();
        if(!($requestingUser->getId() == $booking->getCustomer()->getId() || $this->isGranted('ROLE_ADMIN'))) {
            throw new AccessDeniedHttpException();
        }

        $html = $this->renderView('AppBundle::bill.html.twig', array(
            'booking' => $booking
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="file.pdf"'
            )
        );
    }
}
