<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 29.12.2016
 * Time: 20:08
 */

namespace AppBundle\Controller;


use AppBundle\Entity\State;
use AppBundle\Repository\StateRepository;
use AppBundle\Form\Type\StateType;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StateController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getStatesAction(Request $request)
    {
        $headers = $request->headers->all();
        if (array_key_exists('orderby',$headers)){
            $state = $this->getDoctrine()->getRepository('AppBundle:State')->orderBy($headers['orderby'][0]);
        }
        else if(array_key_exists('search',$headers)){
           $state = $this->getDoctrine()->getRepository('AppBundle:State')->findBySearch($headers['search'][0]);
        }
        else  {
            $state = $this->getDoctrine()->getRepository('AppBundle:State')->findAll();
        }

        $view = $this->view($state);
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getStateAction(Request $request, $id) {
        try {
            $state = $this->getDoctrine()->getRepository('AppBundle:State')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }

        $view = $this->view($state);
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postStateAction(Request $request)
    {
        if (empty($request)) {
            throw new BadRequestHttpException("Empty Request");
        }

        if ($request->query->has('default') && $request->query->getBoolean('default')) {
            $this->resetCurrentDefaultState();
        }

        $state = new State();
        $form = $this->createForm(StateType::class, $state);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($state);
            $em->flush();
        } catch (Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $data = array("message" => "State successfully created!", "state" => $state);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patchStateAction(Request $request, $id) {
        try {
            $state = $this->getDoctrine()->getRepository('AppBundle:State')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
        if ($state == null){
            throw new NotFoundHttpException();
        }

        if ($request->query->has('default') && $request->query->getBoolean('default')) {
            $this->resetCurrentDefaultState();
        }

        $form = $this->createForm(StateType::class, $state);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($state);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException($e->getMessage());
        }

        $data = array("message" => "State successfully updated!", "state" => $state);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function patchStateDefaultAction($id)
    {
        $state = $this->getDoctrine()->getRepository('AppBundle:State')->findOneBy(array('id' => $id));

        if (!$state) {
            throw new NotFoundHttpException();
        }

        $state->setDefault(true);
        $this->resetCurrentDefaultState();

        try {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($state);
            $em->flush();
        } catch (Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }

    private function resetCurrentDefaultState()
    {
        $currentDefaultState = $this->getDoctrine()->getRepository('AppBundle:State')
            ->findOneBy(array('default' => true));

        if (isset($currentDefaultState)) {
            $currentDefaultState->setDefault(false);
        }

        try {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($currentDefaultState);
        } catch (Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }
}