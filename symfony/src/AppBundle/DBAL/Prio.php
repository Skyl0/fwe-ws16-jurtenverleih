<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 29.12.2016
 * Time: 17:37
 */

namespace AppBundle\DBAL;


class Prio extends PrioType
{
    protected $name = 'prio';
    protected $values = array('success', 'info', 'warning', 'danger');
}