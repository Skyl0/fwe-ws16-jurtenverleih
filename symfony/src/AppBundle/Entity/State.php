<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 29.12.2016
 * Time: 17:32
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Type;
use AppBundle\DBAL\Prio;
Type::addType('prio', 'AppBundle\DBAL\Prio');

/**
* State
*
 * @ORM\Table(name="state")
* @ORM\Entity(repositoryClass="AppBundle\Repository\StateRepository")
*/
class State
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var prio
     * @ORM\Column(name="prio",type="prio", columnDefinition="enum('success', 'info', 'warning', 'danger')")
     */
    private $prio = "success";

    /**
     * @var
     * @ORM\Column(name="`default`", type="boolean")
     */
    private $default = false;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return prio
     */
    public function getPrio()
    {
        return $this->prio;
    }

    /**
     * @param prio $prio
     */
    public function setPrio($prio)
    {
        $this->prio = $prio;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param mixed $default
     * @return State
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }
}