<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 26.11.2016
 * Time: 17:13
 */

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use ProductBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product
 *
 * @ORM\Table(name="reserved_categorys")
 *
 * @ORM\Entity()
 */

class CategoryBookingReference
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Booking
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Booking", inversedBy="categorys")
     * @ORM\JoinColumn(referencedColumnName="id", name="booking")
     *
     */
    private $booking;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Category")
     * @ORM\JoinColumn(referencedColumnName="id", name="category")
     *
     */
    private $category;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=false)
     */
    private $amount = 1;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createDate",type="datetime")
     */
    private $timestamp;

    public function __construct()
    {
        $this->timestamp = new \DateTime('now');
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Booking|CategoryBookingReference
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @param Booking $booking
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}