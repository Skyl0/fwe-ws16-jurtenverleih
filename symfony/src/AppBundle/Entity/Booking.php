<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints\OnlyDifferentProducts;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ProductBundle\Validator\Constraints\OnlyDifferentCategories;
use AppBundle\Validator\Constraints\AvailableCategories;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Booking
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookingRepository")
 */
class Booking
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     *
     *
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime")
     *
     *
     */
    private $dueDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createDate",type="datetime")
     */
    private $timestamp;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Customer")
     */
    private $customer;

    /**
     * @var Collection
     *
     * @OnlyDifferentProducts()
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductBookingReference", mappedBy="booking", cascade={"persist"})
     * @ORM\JoinTable(name="booked_products")
     */
    private $products = null;

    /**
     * @var Collection
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OnlyDifferentCategories()
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CategoryBookingReference", mappedBy="booking", cascade={"persist"})
     * @ORM\JoinTable(name="reserved_categorys")
     */
    private $categorys = null;

    /**
     * @var double
     *
     * @ORM\Column(name="total_price", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $totalPrice = null;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\StateHistory", cascade={"persist"})
     *
     */
    private $currentState;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StateHistory", mappedBy="booking", cascade={"persist"})
     * @ORM\JoinTable(name="state_history")
     */
    private $stateHistory;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    public function __construct()
    {
        $this->stateHistory = new ArrayCollection();
        $this->categorys = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->startDate = new \DateTime();
        $this->dueDate = new \DateTime();
        $this->timestamp = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $startDate
     *
     * @return Booking
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return Booking
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set customerId
     *
     * @param int $customer
     *
     * @return Booking
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Get customerId
     *
     * @return int
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set products
     *
     * @param array $products
     *
     * @return Booking
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }


    public function addProduct(ProductBookingReference $productBookingReference)
    {
        $productBookingReference->setBooking($this);
        $this->products->add($productBookingReference);
        $this->calculateTotalPrice();
    }

    public function removeProduct(ProductBookingReference $productBookingReference)
    {
        if ($this->products->contains($productBookingReference)) {
            $this->products->removeElement($productBookingReference);
            $this->calculateTotalPrice();
        }
    }

    /**
     * Get categorys
     *
     * @return Collection|ArrayCollection
     */
    public function getCategorys()
    {
        return $this->categorys;
    }

    /**
     * Set products
     *
     * @param array $categorys
     *
     * @return Booking
     */
    public function setCategorys($categorys)
    {
        $this->categorys = $categorys;

        return $this;
    }

    public function addCategory(CategoryBookingReference $categoryBookingReference)
    {
        $categoryBookingReference->setBooking($this);
        if (!$this->categorys->contains($categoryBookingReference)) {
            $this->categorys->add($categoryBookingReference);
        }
    }

    public function removeCategory(CategoryBookingReference $categoryBookingReference)
    {
        if ($this->categorys->contains($categoryBookingReference)) {
            $this->categorys->removeElement($categoryBookingReference);
        }
    }

    /**
     * Get products
     *
     * @return Collection|ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set totalPrice
     *
     * @param double $totalPrice
     *
     * @return Booking
     */
    private function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return double
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Calculates the total price of all booked products
     */
    public function calculateTotalPrice()
    {
        $this->totalPrice = 0.0;
        $lendingDays = $this->startDate->diff($this->dueDate)->days;
        foreach ($this->products as $product) {
            $this->totalPrice += $product->getPrice() * $product->getAmount() * $lendingDays;
        }
    }

    /**
     * Set state
     *
     * @param $stateHistory
     * @return Booking
     */
    public function setStateHistory($stateHistory)
    {
        $this->stateHistory = $stateHistory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->currentState;
    }

    /**
     * @param mixed $state
     * @return Booking
     */
    public function setState(StateHistory $currentState)
    {
        if (isset($this->currentState)) {
            $this->addStateHistory($this->currentState);
        }
        $currentState->setBooking($this);
        $currentState->setTimestamp(new DateTime());
        $this->currentState = $currentState;
        return $this;
    }

    /**
     * Get state
     *
     * @return ArrayCollection|Collection
     */
    public function getStateHistory()
    {
        return $this->stateHistory;
    }

    public function addStateHistory(StateHistory $stateHistory)
    {
        $this->stateHistory->add($stateHistory);
    }

    public function removeStateHistory(StateHistory $stateHistory)
    {
        if ($this->stateHistory->contains($stateHistory)) {
            $this->stateHistory->removeElement($stateHistory);
        }
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Booking
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}

