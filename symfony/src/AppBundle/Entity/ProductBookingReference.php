<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 26.11.2016
 * Time: 16:11
 */

namespace AppBundle\Entity;
use ProductBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
* Product
*
* @ORM\Table(name="booked_products")
* @ORM\Entity()
*/
class ProductBookingReference
{
    /**
     * @var
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Booking
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Booking", inversedBy="products")
     * @ORM\JoinColumn(name="booking", referencedColumnName="id")
     */
    private $booking;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Product")
     * @ORM\JoinColumn(name="product", referencedColumnName="id")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount = 1;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Staff")
     */
    private $staff;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createDate",type="datetime")
     */
    private $timestamp;

    /**
     * ProductBookingReference constructor.
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime('now');
 //       $this->price = $this->getProduct()->getPrice();
//        $this->getBooking()->calculatePrice();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ProductBookingReference
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return ProductBookingReference
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param Booking $booking
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
        $this->price = $this->product->getCategory()->getPrice();
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function setProductId($product)
    {
        $this->setProduct($product);
    }

    public function getProductId()
    {
        return $this->product;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param int $staff
     */
    public function setStaff($staff)
    {
        $this->staff = $staff;
    }

    /**
     * @return int
     */
    public function getStaff()
    {
        return $this->staff;
    }

}