<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 12.12.2016
 * Time: 00:50
 */

namespace ProductBundle\Form\Type;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductGroupPartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'ProductBundle\Entity\Category',
                'choice_label' => 'name'
            ))
            ->add('amount', IntegerType::class, array(
                'required' => true
            ))
            ->
            add('name', TextType::class, array(
                'required' => false,
                'mapped' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => 'ProductBundle\Entity\ProductGroupPart',
        ));
    }
}