<?php
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 06.12.2016
 * Time: 14:00
 */

namespace ProductBundle\Form\Type;

use AppBundle\Form\Type\StateHistoryType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, array(
                'required'=>true
            ))
            ->add('amount', IntegerType::class, array(
                'required'=>true
            ))
            ->add('code', TextType::class, array(
                'required'=>true
            ))
            ->add('category', EntityType::class, array(
                'class'=>'ProductBundle\Entity\Category',
                'required'=>true
            ))
            ->add('state', StateHistoryType::class, array(
                'required' => false,
                'by_reference' => false
            ))
            ->add('comment', TextType::class, array(
                'required'=>false
            ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>'ProductBundle\Entity\Product',
            'csrf_protection'=>false
        ));
    }
}
