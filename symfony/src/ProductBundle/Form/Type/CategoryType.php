<?php

namespace ProductBundle\Form\Type;

/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 30.11.2016
 * Time: 16:47
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class CategoryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('name', TextType::class, array(
            'required'=>true
        ))
        ->add('price', MoneyType::class, array(
            'required' => true
        ));
}

    public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults(array(
        'data_class'=>'ProductBundle\Entity\Category',
        'csrf_protection'=>false
    ));
}
}