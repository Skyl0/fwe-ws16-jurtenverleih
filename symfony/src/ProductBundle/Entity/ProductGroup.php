<?php

namespace ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use ProductBundle\Validator\Constraints as ProductAssert;

/**
 * ProductGroup
 *
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="product_group")
 * @ORM\Entity(repositoryClass="ProductBundle\Repository\ProductGroupRepository")
 */
class ProductGroup
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Collection
     * @Serializer\Expose()
     * @ProductAssert\OnlyDifferentCategories()
     * @ORM\OneToMany(targetEntity="ProductBundle\Entity\ProductGroupPart", mappedBy="group", cascade={"persist"})
     */
    private $parts;

    /**
     * @var float
     * @Serializer\Expose()
     * @ORM\Column(name="total_price", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $totalPrice;

    public function __construct()
    {
        $this->parts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parts
     *
     * @param array $parts
     *
     * @return ProductGroup
     */
    public function setParts($parts)
    {
        $this->parts = $parts;

        return $this;
    }

    /**
     * Get parts
     *
     * @return array
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return ProductGroup
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function calculateTotalPrice()
    {
        $this->totalPrice = 0.0;
        foreach ($this->parts as $part) {
            $this->totalPrice += $part->getCategory()->getPrice() * $part->getAmount();
        }
    }

    /**
     * @param float $totalPrice
     * @return ProductGroup
     */
    public function setTotalPrice($totalPrice): ProductGroup
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    public function addPart(ProductGroupPart $productGroupPart)
    {
        $productGroupPart->setGroup($this);
        $this->parts->add($productGroupPart);
        $this->calculateTotalPrice();

    }

    public function removePart(ProductGroupPart $productGroupPart)
    {
        $this->parts->removeElement($productGroupPart);
        $this->calculateTotalPrice();
    }
}

