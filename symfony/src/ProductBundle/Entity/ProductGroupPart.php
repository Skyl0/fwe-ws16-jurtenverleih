<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 11.12.2016
 * Time: 18:33
 */

namespace ProductBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProductGroupPart
 * @package ProductBundle\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="product_group_part")
 * @ORM\Entity(repositoryClass="ProductBundle\Repository\ProductGroupPartRepository")
 */
class ProductGroupPart
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ProductGroup
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\ProductGroup", inversedBy="parts")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var int
     * @Serializer\Expose()
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Category", inversedBy="productGroupParts")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var int
     * @Serializer\Expose()
     * @ORM\Column(name="amount", type="integer", nullable=false)
     */
    protected $amount;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductGroupPart
     */
    public function setId(int $id): ProductGroupPart
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ProductGroup
     */
    public function getGroup(): ProductGroup
    {
        return $this->group;
    }

    /**
     * @param ProductGroup $group
     * @return ProductGroupPart
     */
    public function setGroup(ProductGroup $group): ProductGroupPart
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return ProductGroupPart
     */
    public function setCategory(Category $category): ProductGroupPart
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return ProductGroupPart
     */
    public function setAmount(int $amount): ProductGroupPart
    {
        $this->amount = $amount;
        return $this;
    }

    public function addProductGroup(ProductGroup $productGroup)
    {
        $debug = 0;
    }
}