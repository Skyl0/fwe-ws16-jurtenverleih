<?php

namespace ProductBundle\Entity;

use AppBundle\Controller\StateController;
use AppBundle\Entity\State;
use AppBundle\Entity\StateHistory;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="ProductBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductBookingReference", mappedBy="product")
     * @ORM\JoinTable(name="booked_products")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_FK", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\StateHistory", cascade={"persist"})
     */
    private $currentState;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StateHistory", mappedBy="product", cascade={"persist"})
     * @ORM\JoinTable(name="state_history")
     */
    private $stateHistory;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createDate",type="datetime")
     */
    private $timestamp;

    /**
     * ProductBookingReference constructor.
     */
    public function __construct()
    {
        $this->stateHistory = new ArrayCollection();
        $this->timestamp = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getProductId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Product
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set cid
     *
     * @param integer $category
     *
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get cid
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->currentState;
    }

    /**
     * @param mixed $currentState
     * @return Product
     */
    public function setState(StateHistory $currentState)
    {
        if (isset($this->currentState)) {
            $this->addStateHistory($this->currentState);
        }
        $currentState->setProduct($this);
        $this->currentState = $currentState;
        return $this;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Product
     */
    public function setStateHistory($state)
    {
        $this->stateHistory = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return ArrayCollection|Collection
     */
    public function getStateHistory()
    {
        return $this->stateHistory;
    }

    public function addStateHistory(StateHistory $stateHistory)
    {
        $this->stateHistory->add($stateHistory);
    }

    public function removeStateHistory(StateHistory $stateHistory)
    {
        if ($this->stateHistory->contains($stateHistory)) {
            $this->stateHistory->removeElement($stateHistory);
        }
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->getCategory()->getPrice();
    }

    /**
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}

