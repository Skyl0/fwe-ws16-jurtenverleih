<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 11.12.2016
 * Time: 15:08
 */

namespace ProductBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use ProductBundle\Entity\ProductGroup;
use ProductBundle\Entity\ProductGroupPart;
use ProductBundle\Form\Type\ProductGroupType;
use ProductBundle\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductGroupController extends FOSRestController
{
    public function getProductgroupsAction()
    {
        $productGroups = $this->getDoctrine()->getRepository('ProductBundle:ProductGroup')->findAll();

        $view = $this->view($productGroups, Response::HTTP_OK);
        return $this->handleView($view);
    }

    public function getProductgroupAction($productGroupId)
    {
        $productGroup = $this->getDoctrine()
            ->getRepository('ProductBundle:ProductGroup')
            ->findOneBy(array('id' => $productGroupId));

        $view = $this->view($productGroup, Response::HTTP_OK);
        return $this->handleView($view);
    }

    public function postProductgroupAction(Request $request)
    {
        $productGroup = new ProductGroup();
        $form = $this->createForm(ProductGroupType::class, $productGroup);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        try {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($productGroup);
            $em->flush();
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }

    public function patchProductgroupAction(Request $request, $productGroupId)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $productGroup = $em->getRepository("ProductBundle:ProductGroup")
            ->findOneBy(array('id' => $productGroupId));

        $form = $this->createForm(ProductGroupType::class, $productGroup);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            $view = $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        try {
            $productGroup->calculateTotalPrice();
            $em->persist($productGroup);
            $em->flush();
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }
    }

    public function deleteProductgroupAction($productGroupId)
    {
        if (!($this->isGranted('ROLE_ADMIN'))) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();
        $productGroup = $em->getRepository("ProductBundle:ProductGroup")
            ->findOneBy(array('id' => $productGroupId));
        if (!$productGroup) {
            throw new NotFoundHttpException();
        }

        try {
            $em->remove($productGroup);
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception);
        }

        $view = $this->view(null, Response::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}