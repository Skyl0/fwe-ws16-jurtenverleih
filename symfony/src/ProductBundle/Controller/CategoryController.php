<?php

namespace ProductBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use ProductBundle\Entity\Category;
use ProductBundle\Form\Type\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends FOSRestController
{
    /**
     * @Route("/")
     *
    public function indexAction()
    {
        return $this->render('ProductBundle:Default:index.html.twig');
    }*/

    /**
     * REST action which returns all Categorys
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCategoriesAction(Request $request)
    {
        $headers = $request->headers->all();
        if(array_key_exists('search',$headers)) {
            $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->findBySearch($headers['search'][0]);
        }
        else  {
                $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->findAll();
        }
        $view = $this->view($category);
        return $this->handleView($view);
    }

    /**
     * REST action which returns a Category by id.
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCategoryAction(Request $request, $id) {
        try {
            $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }

        $view = $this->view($category);
        return $this->handleView($view);
    }

    /**
     * REST action which creates a new Category.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postCategoriesAction(Request $request)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        $category = new Category();

        //Validiere Category
        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

//Todo: Category name unic?
        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
        } catch (Exception $e) {
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Category successfully created!", "category" => $category);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
     * REST action which deletes an existing category.
     */
    public function deleteCategoryAction(Request $request, $id) {
        if (!$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        try {
            $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
        if ($category == null){
            throw new NotFoundHttpException();
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Category successfully deleted!");
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
     * REST action which partially updates an existing category.
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patchCategoriesAction(Request $request, $id) {
        if (!$this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        try {
            $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
        if ($category == null){
            throw new NotFoundHttpException();
        }

        //Validiere Category
        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Category successfully updated!", "category" => $category);
        $view = $this->view($data);
        return $this->handleView($view);
    }

}
