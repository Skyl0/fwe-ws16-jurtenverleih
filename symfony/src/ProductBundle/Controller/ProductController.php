<?php

namespace ProductBundle\Controller;

use AppBundle\Entity\StateHistory;
use FOS\RestBundle\Controller\FOSRestController;
use ProductBundle\Entity\Category;
use ProductBundle\Entity\Product;
use ProductBundle\Form\Type\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\VarDumper\VarDumper;

/**
 * RestAPI: Product.
 *
 */
class ProductController extends FOSRestController
{
    /**
     * REST GET All
     */
    public function getProductsAction(Request $request)
    {
        $vardumper = new VarDumper();
        $vardumper->dump($request);
        $headers = $request->headers->all();
        if(array_key_exists('search',$headers)) {
            $products = $this->getDoctrine()->getRepository('ProductBundle:Product')->findBySearch($headers['search'][0]);
        } else if(array_key_exists('code',$headers)) {
            $products = $this->getDoctrine()->getRepository('ProductBundle:Product')->findByCode($headers['code'][0]);
        } else {
            $products = $this->getDoctrine()->getRepository('ProductBundle:Product')->findAll();
        }
        $view = $this->view($products)->setStatusCode(200);
        return $this->handleView($view);
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getProductAction(Request $request, $id) {
        try {
            $product = $this->getDoctrine()->getRepository('ProductBundle:Product')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }

        $view = $this->view($product);
        return $this->handleView($view);
    }

    /**
     * REST action which creates a new product.
     *
     */
    public function postProductAction(Request $request) {
        if ($this->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        $product = new Product();

        //Category suchen
        $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->findOneBy(array('id' => $request->request->get('category')));
        if (!$category) {
            throw new NotFoundHttpException('CategoryID not found.');
        }

        //Validiere Product
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        $defaultState = $this->getDoctrine()->getRepository('AppBundle:State')
            ->findOneBy(array('default' => true));

        if (isset($defaultState)) {
            $initialState = new StateHistory();
            $initialState->setUser($this->getUser());
            $initialState->setState($defaultState);
            $initialState->setComment('Product created');
            $product->setState($initialState);
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Product successfully created!", "product" => $product);
        $view = $this->view($data);
        return $this->handleView($view);
    }


    /**
     * REST action which partially updates an existing task.
     *
     */
    public function patchProductAction(Request $request, $id)
    {
        try {
            /** @var Product $product */
            $product = $this->getDoctrine()->getRepository('ProductBundle:Product')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
        //TOdo: Categorie ändern geht noch nciht
        if (array_key_exists('category', $request->request)) {
            die("h:".$request->request->get('category')['id']);
            //Category suchen
            $category = $this->getDoctrine()->getRepository('ProductBundle:Category')->findOneBy(array('id' => $request->request->get('category')));
            if ($category == null) {
                throw new NotFoundHttpException('CategoryID not found.');
            }
            $request->request->set('category', $category->getId());
        }

        //Validiere Product
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($request->request->all(),false);
        if (!$form->isValid()) {
            $view = $this->view($form->getErrors());
            return $this->handleView($view);
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Product successfully updated!", "product" => $product);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
     * REST action which deletes an existing Product.
     */
    public function deleteProductAction(Request $request, $id) {
        try {
            /** @var Product $product */
            $product = $this->getDoctrine()->getRepository('ProductBundle:Product')->find($id);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }

        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }catch(Exception $e){
            throw new BadRequestHttpException();
        }

        $data = array("message" => "Product successfully deleted!");
        $view = $this->view($data);
        return $this->handleView($view);
    }


}
