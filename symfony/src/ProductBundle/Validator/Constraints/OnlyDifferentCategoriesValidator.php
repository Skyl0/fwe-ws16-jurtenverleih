<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 23.12.2016
 * Time: 12:03
 */

namespace ProductBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OnlyDifferentCategoriesValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $parts = $value->getValues();
        $categoryIDs = array();
        foreach ($parts as $part) {
            $categoryIDs[] = $part->getCategory()->getId();
        }
        $uniqueCategoryIDs = array_unique($categoryIDs);

        if (count($uniqueCategoryIDs) < count($categoryIDs)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}