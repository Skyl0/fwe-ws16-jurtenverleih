<?php
/**
 * Created by PhpStorm.
 * User: Steffen
 * Date: 23.12.2016
 * Time: 12:02
 */

namespace ProductBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class OnlyDifferentCategories
 * @package ProductBundle\Validator\Constraints
 * @Annotation()
 */
class OnlyDifferentCategories extends Constraint
{
    public $message = 'The product group contains the same category multiple times';
}