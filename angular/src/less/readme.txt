Für die Linenumbers in der generierten CSS Datei zum wiederfinden in den LESS Dateien.

Development Arguments : --no-color $FileName$ --line-numbers=comments
Production Arguments : --no-color $FileName$ --clean-css="--compatibility=ie8 --advanced"

Man kann 2 Less Compiler anlegen mit 2 Configs und tauschen!