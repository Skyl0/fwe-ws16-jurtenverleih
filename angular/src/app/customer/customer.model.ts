export class Customer {
    constructor(
        public id: number,
        public lastname: string,
        public givenname:string,
        public username: string,
        public email: string,
        public company: string,
        public roles: string[]
    ){}
}

