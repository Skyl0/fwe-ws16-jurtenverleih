import {Component, OnInit} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';
import {Observable} from 'rxjs/Observable';
import {CustomerService} from '../_services/customer/customer.service';
import {Customer} from './customer.model';
import {UserService} from "../_services/user/user.service";
import {SpinnerService} from "../_services/spinner/spinner.service";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  private customerList: Customer[] = [];
  private loaded = false;


  constructor(private http: Http, private cus: CustomerService, private usr: UserService, private spinner : SpinnerService) {
  }

  ngOnInit() {
    this.getCustomers()
  }

  getCustomers() {
    this.spinner.activate('Lade Kunden ...');

    this.cus.getCustomers().subscribe(
      customers => {
        this.customerList = customers;
        //console.log(customers);
        this.spinner.hide();
      },
      error => {
        console.log(error.json);
      });

    //console.log(this.customerList);

  }

  hasRole(role: string[]) {
    return this.usr.isMemberOfRole(role);
  }

}
