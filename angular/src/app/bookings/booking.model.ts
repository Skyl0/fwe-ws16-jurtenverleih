import {Category} from "../categories/category.model";
import {User} from "../_services/user/user.model";
import {Customer} from "../customer/customer.model";
import {State} from "../state/state.model";
import {Product} from "../products/product.model";
/**
 * Created by Joe on 12.01.2017.
 */
export class Booking {
    constructor(
        public id: number,
        public comment: string,
       // public orders: Order[],
        public state_history: State[],
        public start_date: Date,
        public due_date: Date,
        public categorys: Category[],
        public products: Product[],
        public customer: Customer,
        public current_state: State
    ){}
}
/*Category m:n Strucktur*/
export class Order {
    constructor(
        public id: number,
        public category: Category,
        public amount: number,
        public timestamp: Date
    ){}
}
