import {Component, OnInit, NgZone, Input} from '@angular/core';
import {Booking} from "./booking.model";
import {BookingsService} from "../_services/bookings/bookings.service";
import {SpinnerService} from "../_services/spinner/spinner.service";


declare var jQuery: any;

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  private bookingList: Booking[] = [];
    sort: any = {
        column: 'Name', //to match the variable of one of the columns
        descending: false
    };

    selectedClass(columnName){
        return columnName == this.sort.column ? 'sort-' + this.sort.descending : false;
    }

    changeSorting(columnName): void{
        var sort = this.sort;
        if (sort.column == columnName) {
            sort.descending = !sort.descending;
        } else {
            sort.column = columnName;
            sort.descending = false;
        }
    }

    convertSorting(): string{
        return this.sort.descending ? '-' + this.sort.column : this.sort.column;
    }

  constructor(private zone: NgZone, private bookingService: BookingsService, private spin: SpinnerService) { }

  ngOnInit() {
    this.getBookings()
    this.getFocus();
  }

  getBookings() {
    this.spin.activate('Buchungen abrufen ...');

    this.bookingService.getBookings().subscribe(
        bookings => {
          this.bookingList = bookings;
          console.log(bookings);
          this.spin.hide();
        },
        error => {
          console.log(error.json);
          this.spin.hide();
        });

  }



  getFocus() {
    jQuery('#bookings-input').focus();
  }


}
