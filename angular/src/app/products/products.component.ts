import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../_services/products/products.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {Product} from "./product.model";
import {UserService} from "../_services/user/user.service";
import {ShoppingCartService} from "../_services/shoppingcart/shopping-cart.service";
import {Category} from "../categories/category.model";
import {CategoriesService} from "../_services/categories/categories.service";
import {Headers} from "@angular/http";

declare var jQuery: any;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  private products: Product[] = [];

  private searchText: string = '';

  private newProd = {
    'name': '',
    'code': '',
    'amount': 0,
    'category': 0

  };

  private emptyProd = {
    'name': '',
    'code': '',
    'amount': 0,
    'category': 0,

  };

  private current_id;

  private categories: Category[];

  constructor(private prods: ProductsService, private spinner: SpinnerService, private usr: UserService,
              private shopc: ShoppingCartService, private cats: CategoriesService) {
  }

  toggleDiv() {
    jQuery('.jQ-div-toggle').slideToggle();
  }

  ngOnInit() {
    this.getProducts();
    this.getCategories();
  }

  getProducts() {
    console.group("getProducts");
    let headers = new Headers();
    headers.append('search', this.searchText);

    this.prods.getProducts(headers).subscribe(
      data => {
        this.products = data;
        this.spinner.hide();
      //  console.log(this.products);
      },
      error => {
        console.error(error)
      });
    console.groupEnd();
  }

  getCategories() {
    this.cats.getCategories().subscribe(
      data => {
        this.categories = data;
        this.spinner.hide();
      },
      error => {
        console.log(error);
      }
    )
  }

  postProduct() {
    this.cats.getCategory(this.current_id).subscribe(
      category => {
        this.newProd.category = category.id;
        this.prods.postProduct(this.newProd).subscribe(
          data => {
            this.newProd = this.emptyProd;
            this.getProducts();
          },
          error => {
            console.error(error);
          }
        )
      },
      error => {
        console.error(error);
      }
    );
  }

  selectCategory(id: number) {
    this.current_id = id;
  }

}
