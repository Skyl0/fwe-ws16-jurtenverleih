import {Category} from "../categories/category.model";
import {State} from "../state/state.model";
export class Product {
  constructor(
    public id: number,
    public name: string,
    public code: string,
    public current_state: State,
    public price: number,
    public category: Category,
    public product: Product,
    public amount: number,
    public comment: string,
    public state_history: any[]
  ){}
}