import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  private type = '';
  private output = '';

  constructor(private actRoute: ActivatedRoute) {
    this.type =  actRoute.snapshot.data[0]['type'];
    console.log(this.type);
    switch (this.type) {
      case 'unauthorized': this.output = "401 - Nicht autorisiert"; break;
      case 'notfound': this.output = "404 - Nicht gefunden"; break;
      default: this.output = "Kein Error Code"; break;
    }
  }

  ngOnInit() {
  }

}
