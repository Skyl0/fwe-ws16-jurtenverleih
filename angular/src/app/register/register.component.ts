import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {RegisterService} from '../_services/register/register.service';
import {UserService} from "../_services/user/user.service";
import {Router} from "@angular/router";
import {SpinnerService} from "../_services/spinner/spinner.service";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  cred = {
    'givenname': '',
    'lastname': '',
    'company': '',
    'street': '',
    'zipcode': '',
    'city': '',
    'country': '',
    'phone': '',
    'email': '',
    'password': ''
  };

  pw2 = '';
  result = null;


  constructor(private http: Http, private regServ: RegisterService, private usr: UserService, private router: Router, private spin : SpinnerService) {
  }


  ngOnInit() {
  }

  doRegister() {
    this.spin.activate('Registriere ' + this.cred.givenname + ' ...');
    if (!this.checkPwEqual()) {
      return;
    }

    this.regServ.doRegister(this.cred).subscribe(
      data => {
        localStorage.setItem('id_token',data.token);
        this.usr.refreshUser();
        this.router.navigate(['/']);
      },
      error => {
        console.error("Error registering!");
        return Observable.throw(error);
      }
    );

  }

  checkPwEqual() {
    if (this.cred.password == '') return false;
    return this.cred.password === this.pw2;
  }

}
