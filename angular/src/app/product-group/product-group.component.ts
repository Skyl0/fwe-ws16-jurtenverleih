import {Component, OnInit} from '@angular/core';
import {ProductGroupService} from "../_services/product-group/product-group.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {CategoriesService} from "../_services/categories/categories.service";

declare var jQuery: any;

@Component({
  selector: 'app-product-group',
  templateUrl: './product-group.component.html',
  styleUrls: ['./product-group.component.css']
})
export class ProductGroupComponent implements OnInit {

  private prodGroups: any[] = null;
  private categories: any[] = [];
  private parts: any[] = [];

  private newPG = {
    "name": "",
    "parts": null
  };

  constructor(private cgs: CategoriesService, private pgs: ProductGroupService, private spinner: SpinnerService) {
  }

  ngOnInit() {
    this.getProductGroups();
    this.getCategories();
  }

  toggleDiv() {
    jQuery('.jQ-div-toggle').slideToggle();
  }

  getCategories() {
    this.cgs.getCategories().subscribe(
      cat => {
        this.categories = cat;
        console.log(this.categories);
      },
      error => console.log(error)
    );

  }

  addToParts(id: number) {
    let found: boolean = false;
    let toConvert = this.categories[id];

    for (let item of this.parts) {
      if (item.category == toConvert.id) {
        found = true;
        item.amount += 1;
      }
    }
    if (!found) {

      let newItem = {
        "category": toConvert.id,
        "name": toConvert.name,
        "amount": 1
      };
      this.parts.push(newItem);
    }

    // console.log(this.parts);
  }

  postProductGroup() {
    this.spinner.activate("Erstelle Produktgruppe...");
    this.newPG.parts = this.parts;

    this.pgs.postProductGroup(this.newPG).subscribe(
      pg => {
        // console.log(pg);
        this.getProductGroups();
        this.spinner.hide();
      },
      error => console.error(error)
    );
  }

  clearParts() {
    this.parts = [];
  }

  getProductGroups() {
    this.pgs.getProductGroups().subscribe(
      prodgroups => {
        this.spinner.hide();
        this.prodGroups = prodgroups;
        console.group("ProdGroup GET");
        console.log(this.prodGroups);
        console.groupEnd();
      },
      error => {
        console.error(error);
      }
    )
  }

}
