import {Component, OnInit, NgZone} from '@angular/core';
import {Category} from './category.model';
import {CategoriesService} from "../_services/categories/categories.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {ShoppingCartService} from "../_services/shoppingcart/shopping-cart.service";
import {UserService} from "../_services/user/user.service";
import {ProductGroupService} from "../_services/product-group/product-group.service";

declare var jQuery: any;

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  private categoryList: Category[] = [];
  private prodGroups: any = [];
  private name: string;
  private prio: string;
  private priceList: number[] = [];

  private new = {
    name: '',
    price: 0
  };


  constructor(private pgs: ProductGroupService, private usr: UserService, private shopc: ShoppingCartService, private zone: NgZone, private catService: CategoriesService, private spin: SpinnerService) {
  }

  ngOnInit() {
    this.getCategories();
    this.getProductGroups();
    this.getFocus();
  }

  getProductGroups() {
    this.pgs.getProductGroups().subscribe(
      data => {
        this.prodGroups = data;
        console.log(data);
        
      },
      error => console.log(error.json())
    )
  }

  getPriceForPG() {
    for (let item of this.prodGroups) {
      let sum = 0;
      for (let parts of item) {
        sum += parts.amount * parts.category.price;
      }
      this.priceList.push(sum);
    }
  }

  slideToggle(id : number) {
    jQuery('.partlist-' + id).slideToggle();
  }

  getCategories() {
    this.spin.activate('Kategorien abrufen ...');

    this.catService.getCategories().subscribe(
      categories => {
        this.categoryList = categories;
        console.log(JSON.stringify(categories));
        this.spin.hide();
      },
      error => {
        console.log(error.json);
        this.spin.hide();
      });

  }

  toggleDiv() {
    jQuery('.jQ-div-toggle').slideToggle();
  }

  keyEnter(event) {
    //console.log(event.key);
    if (event.key == 'Enter') this.newCategory();
  }

  getFocus() {

    jQuery('#categories-input').focus();

  }

  newCategory() {
    this.spin.activate('Neue Kategorie anlegen ...');

    this.catService.postCategory(this.new).subscribe(
      categories => {
        //console.log(categories);
        this.spin.hide();
        this.getCategories();
        this.new.name = '';
      },
      error => {
        console.log(error.json);
        this.spin.hide();
      });

  }

}
