import {
  Component,
  OnInit,
  Directive,
  ViewChild,
  NgModule,
  ViewChildren,
  EventEmitter,
  ViewContainerRef
} from '@angular/core';
import {Booking} from "../bookings/booking.model";
import {Router, ActivatedRoute} from "@angular/router";
import {BookingsService} from "../_services/bookings/bookings.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {Product} from "../products/product.model";
import {ProductsService} from "../_services/products/products.service";
import {Headers, Response} from "@angular/http";
import {ReactiveFormsModule} from '@angular/forms';
import {FormBuilder, Validators} from '@angular/forms';
import {Category} from "../categories/category.model";
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
var server = require('../server.js');
import {AuthHttp} from "angular2-jwt";
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import {UserService} from "../_services/user/user.service";
import {FixState, State} from "../state/state.model";
import {StateService} from "../_services/state/state.service";
import {FileService} from "../_services/file/file.service";
import {CountProductPipe} from '../_pipes/CountProduct.pipe';


let fileSaver = require('./FileSaver.js');

@Component({
  selector: 'app-bookings-detail',
  templateUrl: './bookings-detail.component.html',
  styleUrls: ['./bookings-detail.component.css']
})
@Directive({
  selector: 'form[addProduct]'
})

export class BookingsDetailComponent implements OnInit {
  @ViewChild('editStateModal')
  modal1: ModalComponent;

  @ViewChild('addProductsModal')
  modal2: ModalComponent;

  private id = 0;
  public current: Booking = {
    id: 0,
    comment: '',
    // orders: null,
    state_history: [],
    start_date: null,
    due_date: null,
    categorys: [],
    products: [],
    customer: null,
    current_state: null
  };
  private name: string = '';
  private code: string = '';
  private newStateID: number = 0;
  private newStateComment: string = '';
  private states: FixState[] = null;
  private products: Product[] = [];
  private tempProducts: Product[] = [];
  private productSuggestions: Product[] = [];
  private cp: CountProductPipe = new CountProductPipe;


  constructor(private fs: FileService, private ahttp: AuthHttp, public toastr: ToastsManager, vcr: ViewContainerRef, private router: Router, private activatedRoute: ActivatedRoute, private booking: BookingsService
    , private spinner: SpinnerService, private usr: UserService, private prods: ProductsService, private ss: StateService) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getBooking();
    this.getProducts();
    this.getStates();

  }


  getStates() {
    this.ss.getStates().subscribe(
      states => {
        this.states = states;
        console.log(this.states)
      },
      error => {
        console.log(error)
      }
    );
  }

  getBooking() {
    this.spinner.activate('Buchung ' + this.id + ' abrufen ...');

    this.booking.getBooking(this.id).subscribe(
      booking => {
        this.current = booking;
        console.group("booking:126");
        console.log(booking);
        console.groupEnd();
        this.cp.products = booking.products;
        this.spinner.hide();
        //this.initProtCount();
      },
      error => {
        console.log(error.json);
      });
  }

  patchBooking() {
    this.spinner.activate('Aktualisierung von Booking ' + this.id);
    this.booking.patchBooking(this.current, this.id).subscribe(
      booking => {
        console.log('Updated #' + this.current.id);
        this.getBooking();
        this.spinner.hide();
        this.router.navigate(['bookings']);
      },
      error => {
        console.log(error.json)
      });

  }

  patchAddProducts() {
    this.spinner.activate('Produkte werden hinzugefügt');

    let newProducts = [];

    for (let item of this.current.products) {
      console.log(item);
      if (item.product != null) {
        newProducts.push(item.product);
      } else {
        console.log("was null");
      }


    }

    let newArray = this.tempProducts.concat(newProducts);
    // console.log(this.current.products);
    console.log(newArray);

    this.booking.patchAddProducts(newArray, this.id).subscribe(
      booking => {
        console.log('Add Products to #' + this.current.id);
        this.spinner.hide();
        this.getBooking();
        // this.current = booking;
        this.router.navigate(['bookings/' + this.id]);
      },
      error => {
        console.log(error.json)
      });
  }

  /*
   deleteBooking() {
   this.spinner.activate('Lösche ' + this.id + ' ...');
   this.booking.deleteBooking(this.id).subscribe(
   booking => {
   console.log('Deleted #' + this.current.id);
   this.spinner.hide();
   this.router.navigate(['bookings']);
   },
   error => console.log(error.json)

   );

   }*/

  createBill() {

    this.fs.downloadBookingPdf(this.id).subscribe(
      data => {
        console.log(data);
        let file = new Blob([data._body], {type: 'application/pdf', endings: 'transparent'});
        window.open(window.URL.createObjectURL(file));
      },
      error => console.log("Error downloading the file."),
      () => console.log('Completed file download.')
    );

  }

  getProducts() {
    let headers = new Headers();
    //headers.append('search', this.searchText);

    this.prods.getProducts(headers).subscribe(
      data => {
        this.products = data;
      },
      error => {
        console.error(error)
      });
  }

  searchProduct() {

    console.log("searchProduct() input:" + this.code);
    if (this.code.trim().length > 0) {
      var foundSomething = false;
      for (var i = 0; this.products.length > i; i++) {
        if (this.products[i].code == this.code.trim()) {
          this.tempProducts.push(this.products[i]);
          foundSomething = true;
          this.toastr.success('Produkt ' + this.products[i].name + ' gefunden.', 'Success!');
          break;
        }
      }
    } else {
      //Error
      console.error("Search string...");
    }
    if (foundSomething == false) {
      //Error
      console.error("No Products found!");
    } else {
      this.name = "";
      this.code = "";
    }

  }

  searchProductSuggestions() {

    this.productSuggestions = [];
    if (this.name.length >= 3) {
      for (var i = 0; this.products.length > i; i++) {
        //if (this.products[i].name.substr(0,this.name.length) == this.name.trim()){
        if (this.products[i].name.toLowerCase().search(this.name.trim().toLowerCase()) != -1) {
          this.productSuggestions.push(this.products[i]);
        }
      }
    }
    console.log("searchProductSuggestions() input:" + this.name + " treffer:" + this.productSuggestions.length);
  }

  addTempProducts(p: Product) {
    this.tempProducts.push(p);
    this.name = "";
    this.productSuggestions = [];
  }

  removeTempProducts(p: Product) {
    this.tempProducts.splice(this.tempProducts.indexOf(p), 1);

  }

  addProducts() {
    this.products
    for (var i = 0; this.tempProducts.length > i; i++) {
      this.current.products.push(this.tempProducts[i]);
    }
    this.patchAddProducts();
    this.cancelProducts();
  }

  cancelProducts() {
    this.tempProducts = null;
    this.productSuggestions = [];
    this.name = "";
    this.code = "";
  }

  //
  // removeProduct(p: Product) {
  //   this.current.products.splice(this.current.products.indexOf(p), 1);
  //   this.patchAddProducts();
  // }

  removeProduct(i: number) {
    console.group("removeProd #" +i);

    let newProducts = this.current.products;

    console.log(newProducts);
    console.log("newP lenght=" + newProducts.length);
    newProducts.splice(i, 1);
    console.log(newProducts);
    console.log("newP lenght=" + newProducts.length);

    this.current.products = newProducts;

    this.patchAddProducts();
    console.groupEnd();
  }


  newState() {
    this.spinner.activate('Speichere neuen Status für Buchung ' + this.id + ' ...');

    this.booking.patchStateBooking(this.newStateID, this.newStateComment, this.current.id).subscribe(
      booking => {
        // this.current = booking;

        console.log(booking);
        this.spinner.hide();
        this.router.navigate(['bookings/' + this.id]);
      },
      error => {
        console.log(error.json);
      });

    this.newStateID = 0;
    this.newStateComment = '';
    this.getBooking();
  }

  showSuccess() {
    this.toastr.success('You are awesome!', 'Success!');
  }

  showError() {
    this.toastr.error('This is not good!', 'Oops!');
  }

  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }

  showCustom() {
    this.toastr.custom('<span style="color: red">Message in red.</span>', null, {enableHTML: true});
  }
}



