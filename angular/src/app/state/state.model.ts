/**
 * Created by Joe on 25.01.2017.
 */
import {User} from "../_services/user/user.model";
import {Customer} from "../customer/customer.model";

/*eigentliche state_history item*/
export class State {
  constructor(public id: number,
              public state: FixState,
              public comment: string,
              public user: Customer,
              public timestamp: Date) {
  }
}
/*entspricht dem eigentlichem State*/
export class FixState {
  constructor(public id: number,
              public name: string,
              public prio: string,
              public _default: boolean) {
  }
}

