import {Component, OnInit} from '@angular/core';
import {StateService} from "../_services/state/state.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {FixState} from "./state.model";

declare var jQuery: any;

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {

  public stateList: FixState[] = [];
  public newState = {
    name : "",
    prio : ""
  };

  private prios = [ 'success', 'info', 'warning', 'danger' ];

  constructor(private ss: StateService, private spin: SpinnerService) {
  }

  ngOnInit() {
    this.getStates();
  }

  getStates () {
    this.spin.activate("Statusse abrufen...");

    this.ss.getStates().subscribe(
      states => { this.stateList = states;
      console.log(states);
      this.spin.hide();
      },
      error => {
        console.error(error.json);
        this.spin.hide();
      }
    );
  }

  toggleDiv() {
    jQuery('.jQ-div-toggle').slideToggle();
  }

  postState() {
    this.ss.postState(this.newState).subscribe(
      state => {
        this.spin.hide();
        this.getStates();
        this.newState.name= "";
        this.newState.prio= "";
      },
      error => {
        console.log(error.json);
        this.spin.hide();
      }
    )
  }

}
