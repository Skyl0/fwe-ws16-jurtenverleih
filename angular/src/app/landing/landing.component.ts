import {Component, OnInit} from '@angular/core';
import {SpinnerService} from "../_services/spinner/spinner.service";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  protected version = localStorage.getItem('version');

  constructor(private spinner : SpinnerService) {
  }

  ngOnInit() {
  }



}
