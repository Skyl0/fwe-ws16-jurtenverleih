import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, BrowserXhr} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {DatePipe, CurrencyPipe} from "@angular/common";
import {ReactiveFormsModule} from '@angular/forms';

//External
import {Ng2Bs3ModalModule} from 'ng2-bs3-modal/ng2-bs3-modal';
import {AUTH_PROVIDERS} from 'angular2-jwt';
import {DatePickerModule} from 'ng2-datepicker';
import {ToastModule} from 'ng2-toastr/ng2-toastr';

// Services / Pipes / Directives
import {RegisterService} from "./_services/register/register.service";
import {UserService} from "./_services/user/user.service";
import {AuthService} from "./_services/auth/auth.service";
import {CustomerService} from "./_services/customer/customer.service";
import {CategoriesService} from "./_services/categories/categories.service";
import {SpinnerService} from './_services/spinner/spinner.service';
import {FormValuesPipe} from './_pipes/form-values.pipe';
import {AuthGuard} from "./_services/auth/auth-guard";
import {EmitterService} from "./_services/emitter/emitter.service";
import {SpinnerDirective} from './_directives/spinner.directive';
import {ShoppingCartService} from "./_services/shoppingcart/shopping-cart.service";
import {ProductsService} from "./_services/products/products.service";
import {BookingsService} from "./_services/bookings/bookings.service";
import {OrderBy} from "./_pipes/orderBy/orderBy";
import {Format} from "./_pipes/orderBy/format";
import {firstLettersPipe} from "./_pipes/firstLetters";
import {FocusDirective} from "./_directives/focus.directive";
import {StateService} from "./_services/state/state.service";
import {ProductGroupService} from "./_services/product-group/product-group.service"
import {FileService} from "./_services/file/file.service"
import {CountProductPipe} from "./_pipes/CountProduct.pipe";

//Components
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {RegisterComponent} from './register/register.component';
import {LandingComponent} from './landing/landing.component';
import {CustomerComponent} from './customer/customer.component';
import {ErrorComponent} from './error/error.component';
import {CustomerDetailComponent} from './customer-detail/customer-detail.component';
import {CategoriesComponent} from './categories/categories.component';
import {CategoriesDetailComponent} from './categories-detail/categories-detail.component';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {ProductsComponent} from './products/products.component';
import {BookingsComponent} from './bookings/bookings.component';
import {ProfileComponent} from './profile/profile.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {BookingsDetailComponent} from './bookings-detail/bookings-detail.component';
import {StateComponent} from './state/state.component';
import {StateDetailComponent} from './state-detail/state-detail.component';
import {ProductGroupComponent} from './product-group/product-group.component';



/****************
 * Routes BEGIN
 ****************/
const appRoutes: Routes = [
  {
    path: 'cart',
    component: ShoppingCartComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: []
      }
    ]
  },
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'products/:id',
    component: ProductDetailComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: []
      }
    ]
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: []
      }
    ]
  },
  {
    path: 'categories/:id',
    component: CategoriesDetailComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'bookings',
    component: BookingsComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }

    ]
  },
  {
    path: 'bookings/:id',
    component: BookingsDetailComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'customers',
    component: CustomerComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'customers/:id',
    component: CustomerDetailComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'states',
    component: StateComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'states/:id',
    component: StateDetailComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'productgroups',
    component: ProductGroupComponent,
    canActivate: [AuthGuard],
    data: [
      {
        role: ['ROLE_ADMIN']
      }
    ]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'unauthorized',
    component: ErrorComponent,
    data: [{type: 'unauthorized'}]
  },
  {
    path: '**',
    component: ErrorComponent,
    data: [{type: 'notfound'}]
  },
  {
    path: 'start',
    redirectTo: '/'
  }
];

/****************
 * Routes END
 ****************/


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavBarComponent,
    RegisterComponent,
    LandingComponent,
    FormValuesPipe,
    CustomerComponent,
    ErrorComponent,
    CustomerDetailComponent,
    SpinnerDirective,
    CategoriesComponent,
    CategoriesDetailComponent,
    ShoppingCartComponent,
    ProductsComponent,
    ProfileComponent,
    ProductDetailComponent,
    BookingsComponent,
    BookingsDetailComponent,
    OrderBy, Format,
    firstLettersPipe,
    FocusDirective,
    StateComponent,
    StateDetailComponent,
    ProductGroupComponent,
    CountProductPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Bs3ModalModule,
    RouterModule.forRoot(appRoutes),
    DatePickerModule,
    ReactiveFormsModule,
    ToastModule.forRoot()
  ],
  providers: [
    RegisterService,
    UserService,
    AuthService,
    CustomerService,
    EmitterService,
    SpinnerService,
    ShoppingCartService,
    ProductsService,
    AuthGuard,
    FormValuesPipe,
    CurrencyPipe,
    DatePipe,
    AUTH_PROVIDERS,
    CategoriesService,
    BookingsService,
    StateService,
    ProductGroupService,
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
