import {Component, OnInit, NgModule} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {ProductsService} from "../_services/products/products.service";
import {Product} from "../products/product.model";
import {StateService} from "../_services/state/state.service";
import {FixState} from "../state/state.model";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})

export class ProductDetailComponent implements OnInit {

  private id = 0;
  private current: any = {
    'id': 0,
    'name': '',
    'code': '',
    'amount': 0,
    'price': 0,
    'current_state': null,
    'state_history': null,
    'comment': '',
    'category': {
      'id': 0,
      'name': '',
      'price': 0
    }
  };

  private states: FixState[] = null;

  private possible_roles = ['ROLE_CUSTOMER', 'ROLE_STOREMAN', 'ROLE_CONTROLLER', 'ROLE_ADMIN'];

  constructor(private router: Router, private ss: StateService, private activatedRoute: ActivatedRoute, private prods: ProductsService, private spinner: SpinnerService) {

  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getProduct();
    this.getStates();
  }

  getProduct() {
    this.spinner.activate('Lade Produkt ' + this.id + ' ...');
    this.prods.getProduct(this.id).subscribe(
      product => {
        this.current = product;
        console.log(product);
        this.spinner.hide();
      },
      error => {
        console.log(error.json);
      });

  }

  getStates() {
    this.ss.getStates().subscribe(
      states => {
        this.states = states;
        console.log(this.states)
      },
      error => {
        console.log(error)
      }
    );
  }

  patchProduct() {

    let toPatch = {
      "name": this.current.name,
      "amount": this.current.amount,
      "code": this.current.code,
      "category": this.current.category.id,
      "state": {
        "state": this.current.current_state.state.id
      },
      "comment": ""
    };

    this.prods.patchProduct(toPatch, this.id).subscribe(
      product => {
        console.log('Updated #' + this.id);
        this.router.navigate(['/products']);
      },
      error => {
        console.log(error);
      });
  }

  deleteProduct(id: number) {

    this.prods.deleteProduct(id).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/products']);
      },
      error => {
        console.log(error);
      }
    )
  }

}
