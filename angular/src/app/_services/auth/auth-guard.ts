import {Injectable} from '@angular/core';
import {Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {CanActivate} from '@angular/router';
import {AuthService} from './auth.service';
import {UserService} from "../user/user.service";
import {Observable} from "rxjs";
import {AuthHttp} from "angular2-jwt";
var server = require('../../server.js');
import {User} from "../user/user.model";


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private ahttp: AuthHttp, private router: Router, private usr: UserService, private actRoute: ActivatedRoute) {

  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {

    let id = this.usr.getCurrentUserIdByToken();
    let cUser = new User(0, '', [], '');
    let roles = route.data[0]['role'];

    return new Promise((resolve, reject) => {
      this.usr.getUser(id).subscribe(
        response => {


          if (!this.auth.loggedIn()) {
            resolve(false);
            this.router.navigate(['unauthorized']);
          }
          if (roles.length <= 0) {
            resolve(true);
          } else {
            cUser = response;
            let found: boolean = false;

            for (let role of roles) {
              console.log(role);
              if (cUser.roles.indexOf(role) > -1) {
                found = true;
              }
            }

            if (found) {
              resolve(true);
            } else {
              this.router.navigate(['unauthorized']);
              resolve(false);
            }
          }

        },
        error => {
          resolve(false);
          this.router.navigate(['unauthorized']);
        })
    });
  }


}