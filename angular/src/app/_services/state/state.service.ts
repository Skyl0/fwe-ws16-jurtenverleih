import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {FixState} from '../../state/state.model'
import {AuthHttp} from "angular2-jwt";
var server = require('../../server.js');
import {Response, Headers} from "@angular/http";
import {SpinnerService} from "../spinner/spinner.service";

@Injectable()
export class StateService {

  private apiPath = "http://" + server.ip + ":8000/api/states";

  constructor(private ahttp: AuthHttp, private spin: SpinnerService) {
  }

  getStates(): Observable<FixState[]> {
    return this.ahttp.get(this.apiPath)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getState(id: number): Observable<FixState> {
    return this.ahttp.get(this.apiPath + "/" + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  postState(state: any): Observable<FixState> {
    console.log(state);
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.post(this.apiPath, state, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  deleteState (id: number)  {
    return this.ahttp.delete(this.apiPath + "/" + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  setDefaultState(state: any, id: number) {
    return this.ahttp.patch(this.apiPath + "/" + id + "/default",state)
      .map((res:Response)=> res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateState (state: any, id: number): Observable<FixState> {
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.patch(this.apiPath + "/" + id, state, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
