import {Injectable} from '@angular/core';
import {AuthHttp} from "angular2-jwt";
// import {SERVER_IP} from "../../app.component";
import {FormValuesPipe} from "../../_pipes/form-values.pipe";
import {Observable} from "rxjs";
import {Booking} from "../../bookings/booking.model";
import {Response, Headers} from "@angular/http";
import {Product} from "../../products/product.model";
import {forEach} from "@angular/router/src/utils/collection";

var server = require('../../server.js');

@Injectable()
export class BookingsService {

  apiPath = "http://" + server.ip + ":8000/api/bookings";


  constructor(private ahttp: AuthHttp, private formVal: FormValuesPipe) {
  }

  getBookings(): Observable<Booking[]> {

    return this.ahttp.get(this.apiPath)
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  getBooking(id: number): Observable<Booking> {

    return this.ahttp.get(this.apiPath + '/' + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  postBooking(booking: any): Observable<Booking> {
    console.log('postBooking:');
    console.log(booking);
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.post(this.apiPath, booking, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  patchBooking(booking: any, id: number): Observable<Booking> {
    console.group("patchBooking:45");
    console.log(booking);

    var request = {
      'comment': booking.comment
    }

    console.log(request);
    var head = new Headers();
    head.append('Content-Type', 'application/json');
    console.groupEnd();
    return this.ahttp.patch(this.apiPath + "/" + id + "/staff", request, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  patchStateBooking(stateID: number, stateComment: string, id: number): Observable<Booking> {
    console.group('patchStateBooking:');

    var request =
        {
          "state": {
            "state": stateID,
            "comment": stateComment
          }
        }
      ;

    console.log(request);
    var head = new Headers();
    head.append('Content-Type', 'application/json');
    console.groupEnd();

    return this.ahttp.patch(this.apiPath + "/" + id + "/staff", request, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  patchAddProducts(products: Product[], id: number): Observable<Booking> {
    console.log('addProducts:');
    console.log(products);

    var request = '{"products":[';
    for (var i = 0; i < products.length; i++) {
      request = request + '{"product": ' + products[i].id + ', "amount": ' + products[i].amount + '}';
      if (i < products.length - 1) {
        request = request + ',';
      }
    }
    request = request + ']}';
    /*var request = {
     trmpString
     }*/
    // console.log(request);
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.patch(this.apiPath + "/" + id + "/staff", request, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  /*
   {
   "products": [
   {
   "product": 2,
   "amount": 3
   },
   {
   "product": 3
   }
   ],
   "state": 2
   }*/

  /* Gibt es nicht
   deleteBooking(id: number): Observable<Category> {
   console.log('deleteBooking:' + id);

   return this.ahttp.delete(this.apiPath + "/" + id)
   .map((res: Response) => res.json())
   .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   }

   */
}
