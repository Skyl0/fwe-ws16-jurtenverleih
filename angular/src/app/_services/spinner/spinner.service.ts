import {Injectable} from '@angular/core';

@Injectable()
export class SpinnerService {
  private _show: boolean = false;
  private _text: string = 'Lade...';

  public text = this.getText();

  constructor() {
  }

  getText(): string {
    return this.text;
  }

  setText(value: string) {
    this.text = value;
  }

  visible() {
    return this._show;
  }

  activate(value: string) {
    this._show = true;
    this.setText(value);
  }

  hide() {
    this._show = false;
  }

}
