import {Injectable} from '@angular/core';
var server = require('../../server.js');
import {AuthHttp} from "angular2-jwt";
import {Observable} from "rxjs";
import {Response, Headers} from "@angular/http";

@Injectable()
export class ProductGroupService {

  private apiPath: string = "http://" + server.ip + ":8000/api/productgroups";

  constructor(private ahttp: AuthHttp) {
  }

  getProductGroups() {

    return this.ahttp.get(this.apiPath)
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err.json() || "Server Error"))
  }

  postProductGroup(pg: any) {
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.post(this.apiPath, pg, {headers: head})
      .map ((res: Response) => res.json())
      .catch((err:any) => Observable.throw(err.json() || "Server Error"))
  }

}
