import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {User} from './user.model';
import {AuthHttp, JwtHelper} from "angular2-jwt";
import {Response} from "@angular/http";
import {AuthService} from "../auth/auth.service";
import {SpinnerService} from "../spinner/spinner.service";
var server = require('../../server.js');

@Injectable()
export class UserService {

  protected emptyUser: User = {
    id: 0,
    roles: [],
    email: '',
    type: ''
  };

  protected jwth: JwtHelper = new JwtHelper();

  private currentUser: User = this.emptyUser;


  protected apiPath: string = 'http://'+ server.ip +':8000/api/users';

  constructor(private ahttp: AuthHttp, private as : AuthService, private spinner: SpinnerService) {
  }

  getUser(id: number): Observable<User> {

    return this.ahttp.get(this.apiPath + '/' + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  refreshUser() {
    this.getUser(this.getCurrentUserIdByToken()).subscribe(
      user => {
        this.currentUser = user;
        this.spinner.hide();
      },
      error => {
        console.log(error.json);
      });
  }

  getCurrentUserIdByToken(): number {
    if (localStorage.getItem("id_token") !== null) {
      var token = localStorage.getItem('id_token');
      var decoded = this.jwth.decodeToken(token);
      //console.log('getUserByToken -> id -> ' + decoded.id);
      return decoded.id;
    } else {
      return -1;
    }
  }

  logOff() {
    this.currentUser = this.emptyUser;
  }

  getEmail(): string {
    return this.currentUser.email;
  }

  isMemberOfRole(search_roles: string[]) {
    for (let current of search_roles) {
      if (this.currentUser.roles.indexOf(current) > -1) return true;
    }
    return false;
  }

  getCurrentUser(): User {
    return this.currentUser;
  }
}
