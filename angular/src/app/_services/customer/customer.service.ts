import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AuthHttp} from 'angular2-jwt';
import {Customer} from '../../customer/customer.model';
import {FormValuesPipe} from '../../_pipes/form-values.pipe'
var server = require('../../server.js');

@Injectable()
export class CustomerService {

  protected apiPath = "http://"+ server.ip +":8000/api/users";

  constructor(private ahttp: AuthHttp, private formVal: FormValuesPipe) {
  }

  getCustomers() : Observable<Customer[]> {

    return this.ahttp.get(this.apiPath)
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  getCustomer(id : number): Observable<Customer> {

    return this.ahttp.get(this.apiPath + '/' + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  patchCustomer(cus: any, id: number): Observable<Customer> {
    console.log('patchCustomer:');
    console.log(cus);

    let request =  {
      'email': cus.email,
      'given_name': cus.given_name,
      'lastname': cus.lastname
    }

    var head = new Headers();
   head.append('Content-Type', 'application/json');

    return this.ahttp.patch(this.apiPath + '/' + id, request, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
