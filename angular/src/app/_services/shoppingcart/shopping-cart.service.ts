import {Injectable} from '@angular/core';
import {Product} from "../../products/product.model";
import {Category} from "../../categories/category.model";
import {CartObject} from "../../shopping-cart/cartobject.model";
import {UserService} from "../user/user.service";

@Injectable()
export class ShoppingCartService {

  public count = 0;

  private content: CartObject[] = [];
  private pgContent: any[] = [];
  public total: number = 0.0;

  constructor(private usr: UserService) {
    if (localStorage.getItem('basket') != null) {
      this.content = JSON.parse(localStorage.getItem('basket'));
    }
    if (localStorage.getItem('basketPg') != null) {
      this.pgContent = JSON.parse(localStorage.getItem('basketPg'));
    }
      this.updateBasket();
  }

  getTotal(): number {
    return this.total;
  }

  getCount() {
    return this.content.length;
  }

  getBasketPG() {
    return this.pgContent;
  }

  addToBasketPG(item: any) {

    this.pgContent.push(item);
    console.log(this.pgContent);
    this.updateBasket();
  }

  addToBasket(item: Category) {
    console.log(item);
    let newItem: CartObject = {
      "category": 0,
      "price": 0,
      "name": "",
      "amount": 0
    };
    let found: boolean = false;
    newItem.category = item.id;
    newItem.price = item.price;
    newItem.name = item.name;


    console.log(newItem);

    for (let obj of this.content) {
      if (obj.category == newItem.category) {
        obj.amount++;
        found = true;
      }
    }
    if (!found) {
      newItem.amount = 1;
      this.content.push(newItem);
    }
    console.log(newItem);
    this.updateBasket();
  }

  removeItemById(id: number) {
    //if (!id >= this.content.length) {
    this.content.splice(id, 1);
    this.updateBasket();
    //}
  }

  removePGItem(id: number) {
    this.pgContent.splice(id, 1);
    this.updateBasket();
  }

  getItems(): CartObject[] {
    return this.content;
  }

  private updateCount() {
    this.count = 0;
    for (let item of this.content) {
      this.count += item.amount;
    }
    // for (let item of this.pgContent) {
    this.count += this.pgContent.length;
    // }

  }

  private updateTotal() {
    this.total = 0.0;
    for (let item of this.content) {
      console.log(item);
      this.total += item.price * item.amount;
    }
    for (let item of this.pgContent) {
      this.total += item.total_price;
    }
  }

  updateBasket() {
    localStorage.setItem('basket', JSON.stringify(this.content));
    localStorage.setItem('basketPg', JSON.stringify(this.pgContent));
    // console.log(localStorage.getItem('basket'));
    // console.log(localStorage.getItem('basketPg'));
    this.updateCount();
    this.updateTotal();
  }

  emptyBasket() {
    this.content = [];
    this.pgContent = [];
  }


}
