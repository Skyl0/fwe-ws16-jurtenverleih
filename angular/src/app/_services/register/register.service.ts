import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var server = require('../../server.js');
import {FormValuesPipe} from "../../_pipes/form-values.pipe";

@Injectable()
export class RegisterService {

  private apiPath: string = 'http://'+ server.ip +':8000/api/customers/register';

  constructor(private http: Http, private formVal : FormValuesPipe) {
  }

  doRegister(cred) {
   // const body = JSON.stringify(cred);
    console.log(cred);
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    return this.http.post(this.apiPath, this.formVal.transform(cred))
      .map((response: Response) => response.json())
      .catch((err: any) => {
        return Observable.throw(err.json())
      });

  }

  private handleError(error: any) {
    console.log(error);

  }
}
