import {Injectable} from '@angular/core';
import {AuthHttp} from "angular2-jwt";
import {Headers} from "@angular/http";
import {Observable} from "rxjs";
var server = require('../../server.js');

@Injectable()
export class FileService {

  private apiPath = "http://" + server.ip + ":8000/api/bookings/";

  constructor(private ahttp: AuthHttp) {
  }

  downloadBookingPdf(id: number) {
    var head = new Headers();
    // head.append('responseType', 'arraybuffer');
    // head.append('Accept', 'application/pdf');
    return this.ahttp.get(this.apiPath + id + "/bill", {headers: head, responseType: 2})
      .map((res: any) => res)
      .catch(error => Observable.throw(error.json() || "Bad Request"));
  }

}
