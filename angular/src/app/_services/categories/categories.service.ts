import {Injectable} from '@angular/core';
import {AuthHttp} from "angular2-jwt";
import {FormValuesPipe} from "../../_pipes/form-values.pipe";
import {Observable} from "rxjs";
import {Category} from "../../categories/category.model";
import {Response, Headers} from "@angular/http";
var server = require('../../server.js');

@Injectable()
export class CategoriesService {
  apiPath = "http://" + server.ip + ":8000/api/categories";

  constructor(private ahttp: AuthHttp, private formVal: FormValuesPipe) {
  }

  getCategories(): Observable<Category[]> {

    return this.ahttp.get(this.apiPath)
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  getCategory(id: number): Observable<Category> {

    return this.ahttp.get(this.apiPath + '/' + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  postCategory(cat: any): Observable<Category> {
    console.log('postCategory:');
    console.log(cat);
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.post(this.apiPath, cat, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  patchCategory(cat: any, id: number): Observable<Category> {
    console.log('patchCategory:');
    console.log(cat);

    var request = {
      'name' : cat.name
    }
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.patch(this.apiPath + "/" + id, request, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  deleteCategory(id: number): Observable<Category> {
    console.log('deleteCategory:' + id);

    return this.ahttp.delete(this.apiPath + "/" + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
