import { Injectable } from '@angular/core';
import {AuthHttp} from "angular2-jwt";
var server = require('../../server.js');
import {Response, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {Product} from "../../products/product.model";

@Injectable()
export class ProductsService {

  private apiPath: string = 'http://'+ server.ip +':8000/api/products';


  constructor(private ahttp: AuthHttp) { }

  getProducts (header: Headers) {
    return this.ahttp.get(this.apiPath) //, {headers: header})
      .map ((res : Response) => res.json())
      .catch ((err : any) => Observable.throw(err.json()))
  }

  getProduct(id : number) {
    // var headers = new Headers();
    // headers.append('Content-Type','application/json');
    return this.ahttp.get(this.apiPath + '/' + id)
        .map ((res : Response) => res.json())
        .catch ((err : any) => Observable.throw(err.json()))
  }

  patchProduct(prod: any, id: number) {
    console.log('patchProd:');
    console.log(JSON.stringify(prod));

    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.patch(this.apiPath + '/' + id, prod, {headers: head})
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  postProduct (prod: any) {
    var head = new Headers();
    head.append('Content-Type', 'application/json');

    return this.ahttp.post(this.apiPath, prod, {headers: head})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteProduct (id: number) {
    return this.ahttp.delete(this.apiPath + '/' + id)
      .map((res: Response) =>res.json())
      .catch ((error: any) =>Observable.throw(error.json().error || 'Server error'));
  }
}
