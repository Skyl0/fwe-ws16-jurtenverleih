import {Component, OnInit} from '@angular/core';
import {Response, Http} from '@angular/http';
import {Router} from '@angular/router';
import {UserService} from '../_services/user/user.service';
import {FormValuesPipe} from '../_pipes/form-values.pipe';
import {AuthHttp} from 'angular2-jwt';
import {SpinnerService} from "../_services/spinner/spinner.service";
import {Observable} from "rxjs";
var server = require('../server.js');


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginCredentials = {
    'email': '',
    'password': ''
  };
  private loginError : boolean = false;
  apiPath = 'http://' + server.ip + ':8000/api/login';

  constructor(private usr: UserService, private router: Router, private formValPipe: FormValuesPipe, private http: Http, private spinner: SpinnerService) {
  }

  ngOnInit() {

  }

  doLogin() {
    this.loginError = false;
    this.spinner.activate('Login ausführen ...');
    this.logMeIn().subscribe(
      token => {
        this.usr.refreshUser();
        this.router.navigate(['/']);
      },
      err => {
        if (err = "Server error") {
          this.loginError = true;
        }
      }
    )

  }

  key (event : any) {
    console.log(event);
    // (keyup)="key($event);"
  }

  logMeIn() {
    return this.http.post(this.apiPath, this.formValPipe.transform(this.loginCredentials))
      .map((response: Response) => {
        var res = response.json();
        localStorage.setItem('id_token', res.token);
      })
      .catch((error: any) => {
      this.loginError
        this.spinner.hide();
        return Observable.throw(error.json().error || 'Server error');
      });
  }
}
