import {Component, OnInit, Input} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Customer} from "../customer/customer.model";
import {CustomerService} from "../_services/customer/customer.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {

  private id = 0;
  private current: Customer = {
    'id': 0,
    'givenname': '',
    'lastname': '',
    'username': '',
    'email': '',
    'roles': null,
    'company':''
  };

  private possible_roles = ['ROLE_CUSTOMER', 'ROLE_STOREMAN', 'ROLE_CONTROLLER', 'ROLE_ADMIN'];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private cus: CustomerService, private spinner: SpinnerService) {

  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getCustomer();
  }

  getCustomer() {
    this.spinner.activate('Lade Benutzer ' + this.id + ' ...');
    this.cus.getCustomer(this.id).subscribe(
      customer => {
        this.current = customer;
        // console.log(customer);
        this.spinner.hide();
      },
      error => {
        console.log(error.json);
      });

  }

  patchCustomer() {

    var to_patch = {
      'email': this.current.email,
      'givenname': this.current.givenname,
      'lastname': this.current.lastname,
    };

    this.cus.patchCustomer(to_patch, this.id).subscribe(
      customer => {
        console.log('Updated #' + this.current.id),
          this.router.navigate(['customers']);
      },
      error => {
        console.log(error.json);
      });
  }

  toggleSelection(role: string) {

    let position = this.current.roles.indexOf(role);
    if (position > -1) {
      this.current.roles.splice(position, 1);
    } else {
      this.current.roles.push(role);
    }
    console.log(this.current);
  }

}
