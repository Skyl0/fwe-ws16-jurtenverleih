import {Component, OnInit} from '@angular/core';
import {UserService} from "../_services/user/user.service";
import {User} from "../_services/user/user.model";
import {AuthHttp} from "angular2-jwt";
import {Router} from "@angular/router";
var server = require("../server");

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private user: User;
  private apiPath: string = "http://" + server.ip + ":8000/api/users";
  private newData = {
    "email": "",
    "password": "",
    "old_password": ""
  };

  constructor(private usr: UserService, private router: Router, private ahttp: AuthHttp) {
  }

  ngOnInit() {
    this.user = this.usr.getCurrentUser();
    console.log(this.user);
  }

  changePassword() {
    this.newData.email = this.user.email;
    this.ahttp.patch(this.apiPath + "/" + this.user.id, this.newData).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/']);
      },
      error => console.error(error.json())
    );
  }

}
