import {Component, OnInit} from '@angular/core';
import {Product} from "../products/product.model";
import {ShoppingCartService} from "../_services/shoppingcart/shopping-cart.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {CurrencyPipe} from "@angular/common";
import {Category} from "../categories/category.model";
import {Booking} from "../bookings/booking.model";
import {BookingsService} from "../_services/bookings/bookings.service";
import {CartObject} from "./cartobject.model";
import {Router} from "@angular/router";
import {UserService} from "../_services/user/user.service";


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  private contents: CartObject[] = [];
  private pgContents: any[] = [];
  private startDate: any = new Date();
  private endDate: any = new Date();
  private booking = {
    "startDate": null,
    "dueDate": null,
    "categorys": [],
    "comment": ""
  };
  private dateError : boolean = false;

  constructor(private scs: ShoppingCartService,
              private spinner: SpinnerService,
              private curpipe: CurrencyPipe,
              private books: BookingsService,
              private router: Router,
              private usr: UserService) {
  }

  sendOrder() {
    console.log(this.startDate);
    this.booking.startDate = this.startDate.formatted;
    this.booking.dueDate = this.endDate.formatted;
    this.booking.categorys = [];

    this.dateError = false;
    if (!this.booking.startDate || !this.booking.dueDate) {
      this.dateError = true;
    }

    if (!this.dateError)
    {
      for (let pg of this.pgContents) {
        for (let parts of pg.parts) {
          // new
          let newItem = {
            "category": 0,
            "price": 0,
            "name": "",
            "amount": 0
          };
          newItem.category = parts.category.id;
          newItem.price = parts.category.price;
          newItem.name = parts.category.name;
          newItem.amount = parts.amount;
          // console.log(JSON.stringify(newItem));
          let found: boolean = false;

          for (let cat of this.booking.categorys) {
            if (cat.category == newItem.category) {
              console.log("found!");
              found = true;
              cat.amount += newItem.amount;
            }
          }
          if (!found) {
            this.booking.categorys.push(newItem);
          }
        }
      }

      // console.log(this.contents);
      for (let stuff of this.contents) {
        let found: boolean = false;
        // console.log(JSON.stringify(stuff));

        for (let search of this.booking.categorys) {
          // console.group("Search");
          // console.log(search);
          // console.log(stuff);
          // console.groupEnd();

          if (stuff.category == search.category) {
            found = true;
            console.log("found");
            search.amount += stuff.amount;
          }
        }

        if (!found) {
          this.booking.categorys.push(stuff);
        }
      }

      this.books.postBooking(this.booking).subscribe(
        data => {
          console.log(data);
          this.scs.emptyBasket();
          this.scs.updateBasket();
          this.router.navigate(['categories']);
        },
        error => console.error(error)
      );
    }


  }

  ngOnInit() {
    this.getContents();
    // console.log(JSON.stringify(this.contents));

  }

  getContents() {
    this.contents = this.scs.getItems();
    this.pgContents = this.scs.getBasketPG();
    this.booking.categorys = this.contents;
  }

  deleteItem(i: number) {
    this.scs.removeItemById(i);
  }

  deleteItemPg(i: number) {
    this.scs.removePGItem(i);
  }

  message() {

  }

}
