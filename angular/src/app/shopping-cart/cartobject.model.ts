export class CartObject {
  constructor(public category: number,
              public amount: number,
              public price: number,
              public name: string) {
  }
}