import {Component, OnInit} from '@angular/core';
import {Category} from "../categories/category.model";
import {Router, ActivatedRoute} from "@angular/router";
import {CategoriesService} from "../_services/categories/categories.service";
import {SpinnerService} from "../_services/spinner/spinner.service";

@Component({
  selector: 'app-categories-detail',
  templateUrl: './categories-detail.component.html',
  styleUrls: ['./categories-detail.component.css']
})
export class CategoriesDetailComponent implements OnInit {

  private id = 0;
  private current: Category = {
    id: 0,
    name: '',
      price:0
  };

  private loaded = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private cats: CategoriesService
  ,private spinner: SpinnerService) {

  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getCategory();
  }

  getCategory() {
    this.spinner.activate('Kategorie ' + this.id + ' abrufen ...');

    this.cats.getCategory(this.id).subscribe(
      category => {
        this.current = category;
        console.log(category);
        this.spinner.hide();
      },
      error => {
        console.log(error.json);
      });

  }

  patchCategory() {
    this.spinner.activate('Aktualisierung von User ' + this.id);
    this.cats.patchCategory(this.current, this.id).subscribe(
      category => {
        console.log('Updated #' + this.current.id);
        this.spinner.hide();
        this.router.navigate(['categories']);
      },
      error => {
        console.log(error.json)
      });

  }

  deleteCategory() {
    this.spinner.activate('Lösche ' + this.id + ' ...');
    this.cats.deleteCategory(this.id).subscribe(
      customer => {
        console.log('Deleted #' + this.current.id);
        this.spinner.hide();
        this.router.navigate(['categories']);
      },
          error => console.log(error.json)

    );

  }

}
