import {Router} from "@angular/router";
import {Component, OnInit} from '@angular/core';
import {AuthGuard} from '../_services/auth/auth-guard';
import {AuthService} from "../_services/auth/auth.service";
import {UserService} from "../_services/user/user.service";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {ShoppingCartService} from "../_services/shoppingcart/shopping-cart.service";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  private version: string = localStorage.getItem('version');
  private count : number = 0;

  constructor(private auth: AuthService, private router: Router, private authguard: AuthGuard, private usr: UserService,
  private spinner : SpinnerService, private scs : ShoppingCartService) {
  }

  reload() {
    window.location.reload();
  }

  ngOnInit() {
    this.usr.refreshUser();
    this.count = this.scs.getCount();
  }

  doLogOut() {
    this.spinner.activate('Ausloggen...');
    localStorage.removeItem('id_token');
    this.usr.logOff();
    this.router.navigateByUrl('/');
    this.spinner.hide();
  }
}
