import {Component, OnInit} from '@angular/core';

// var variables = require('./server.js');
// export var SERVER_IP : string = variables.ip + ":" + variables.port;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  constructor() {
    localStorage.setItem('version', '1.0');
  }

  ngOnInit () {
   // this.usr.refreshUser();
  }

}

