/**
 * Created by Joe on 30.01.2017.
 */
import { Pipe, PipeTransform } from '@angular/core';
import {Product} from "../products/product.model";
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   catID | countProduct:products[]
 * Example:
 *   {{ 2 |  countProduct:[]}}
 *   formats to: 2
 */
@Pipe({name: 'countProduct'})
export class CountProductPipe implements PipeTransform {
    public products: Product[] = [];

    transform(catID: number, amount: number): any {
        console.log('countProduct:'+ this.products);

        if(amount!=0){
            var div = amount-this.protCounts(catID,this.products);
            if(div < 0) {
                return 'warning';
            } else if (div > 0){
                return 'danger';
            } else {
                return 'success';
            }
        } else {
            return this.protCounts(catID,this.products);
        }

    }

    protCounts(id:number, products: Product[]){
        var count = 0;
        if(products != null){
            for(var k = 0; k < products.length; k++){
                if (products[k].category.id == id){
                    count=count+products[k].amount;
                }
            }
        }
        return count;
    }
}