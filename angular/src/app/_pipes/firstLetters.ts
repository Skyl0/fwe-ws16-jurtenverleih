/**
 * Created by Joe on 15.01.2017.
 */
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'firstLetters'
})
export class firstLettersPipe implements PipeTransform  {


    transform(input:string, number:number): any {
        return (input.length <= number) ? input : input.slice(0,number)+'...';

    }
}