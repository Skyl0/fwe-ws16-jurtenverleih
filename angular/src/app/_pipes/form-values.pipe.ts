import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from "@angular/common";

@Pipe({
  name: 'formValues'
})
export class FormValuesPipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {
  }

  transform(value: any, args?: any): FormData {
    let keyArr: any[] = Object.keys(value);
    let formData = new FormData();

    keyArr.forEach((key: any) => {
      if (value[key] !== null) {
        if (value[key] instanceof Date) {
          value[key] = this.datePipe.transform(value[key], 'dd.MM.yyyy HH:mm');
        }

        formData.append(key, value[key]);
      }
    });

    return formData;
  }

}