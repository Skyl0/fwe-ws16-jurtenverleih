import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SpinnerService} from "../_services/spinner/spinner.service";
import {StateService} from "../_services/state/state.service";

@Component({
  selector: 'app-state-detail',
  templateUrl: './state-detail.component.html',
  styleUrls: ['./state-detail.component.css']
})
export class StateDetailComponent implements OnInit {

  private id = -1;
  private prios = ['success', 'info', 'warning', 'danger'];
  private stateMod = {
    name: "",
    prio: ""
  };
  private default: boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private spinner: SpinnerService, private ss: StateService) {
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.getState();
  }

  getState() {
    this.ss.getState(this.id).subscribe(
      state => {
        console.log(state);
        this.stateMod.name = state.name;
        this.stateMod.prio = state.prio;
      },
      error => {
        console.log(error.json);
      }
    )
  }

  patchState() {
    console.log(this.stateMod);
    this.ss.updateState(this.stateMod, this.id).subscribe(
      state => {
        console.log('Updated #' + this.id),
          this.router.navigate(['/states']);
      },
      error => {
        console.log(error.json);
      });

    if (this.default) {
      this.ss.setDefaultState(this.stateMod, this.id).subscribe(
        res => console.log("New Default #" + this.id),
        error => console.log(error.json)
      );
    }
  }

  changeDefault() {
    this.default = !this.default;
  }

  deleteState() {
    this.ss.deleteState(this.id).subscribe(
      state => {
        console.log('Deleted #' + this.id),
          this.router.navigate(['/states']);
      },
      error => {
        console.log(error.json);
      });

  }

}
