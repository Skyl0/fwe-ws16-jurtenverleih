-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: symfony
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booked_products`
--

DROP TABLE IF EXISTS `booked_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booked_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `price` decimal(7,2) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_159D9EC3E00CEDDE` (`booking`),
  KEY `IDX_159D9EC3D34A04AD` (`product`),
  KEY `IDX_159D9EC3D4D57CD` (`staff_id`),
  CONSTRAINT `FK_159D9EC3D4D57CD` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `FK_159D9EC3D34A04AD` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_159D9EC3E00CEDDE` FOREIGN KEY (`booking`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_products`
--

LOCK TABLES `booked_products` WRITE;
/*!40000 ALTER TABLE `booked_products` DISABLE KEYS */;
INSERT INTO `booked_products` VALUES (1,1,1,NULL,'1.00',1,'2017-01-31 21:22:30'),(2,1,75,NULL,'0.50',1,'2017-01-31 21:22:30'),(3,1,42,NULL,'1.00',1,'2017-01-31 21:22:30'),(4,1,45,NULL,'3.00',1,'2017-01-31 21:22:30'),(5,1,41,NULL,'0.10',50,'2017-01-31 21:22:30'),(6,1,22,NULL,'0.30',10,'2017-01-31 21:22:30');
/*!40000 ALTER TABLE `booked_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `createDate` datetime NOT NULL,
  `total_price` decimal(7,2) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `current_state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E00CEDDE98A046EB` (`current_state_id`),
  KEY `IDX_E00CEDDE9395C3F3` (`customer_id`),
  CONSTRAINT `FK_E00CEDDE9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `FK_E00CEDDE98A046EB` FOREIGN KEY (`current_state_id`) REFERENCES `state_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (1,5,'2017-01-06 00:00:00','2017-01-14 00:00:00','2017-01-31 21:21:50','108.00','Hallo Jörg',137),(2,5,'2017-01-05 00:00:00','2019-01-11 00:00:00','2017-01-31 21:29:45','0.00','#2',140);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(7,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_64C19C15E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Kohtenblatt','1.00'),(2,'Halbdach','2.00'),(3,'Jurtendach','3.00'),(4,'Viereckzeltbahn','0.50'),(5,'Doppelzeltbahn','0.90'),(6,'Jurtenabdeckplane','1.00'),(7,'Jurtenspinne','4.00'),(8,'Steckstange','2.00'),(9,'Abspannseil','0.05'),(10,'Hering','0.10'),(11,'6m Seil','0.30'),(12,'Umlenk-/Blockseil-rolle','0.50'),(13,'4m Mittelstange','2.00'),(14,'Kohten-Abdeckplane','1.00'),(15,'Kohtenkreuz','1.00');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `given_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_81398E09BF396750` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'rolf','huesmann','test','hoffmannstr2','65597','Darmstadt','Germany','0123456789'),(2,'rolfKunde','Kunde','h-da','sdjkfhskjdfkds','65597','sdfdsfd',NULL,'198765'),(5,'Hans','Mustermann','Mustermann GmbH','Tollestraße 1','12345','Darmstadt','DE','12345');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `createDate` datetime NOT NULL,
  `category_FK` int(11) NOT NULL,
  `current_state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D34A04AD98A046EB` (`current_state_id`),
  KEY `IDX_D34A04ADAB8B98D6` (`category_FK`),
  CONSTRAINT `FK_D34A04AD98A046EB` FOREIGN KEY (`current_state_id`) REFERENCES `state_history` (`id`),
  CONSTRAINT `FK_D34A04ADAB8B98D6` FOREIGN KEY (`category_FK`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Kohtenblatt',1,'B01B1NWXQS',NULL,'0000-00-00 00:00:00',1,7),(2,'Kohtenblatt',1,'22137586',NULL,'0000-00-00 00:00:00',1,27),(3,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,28),(4,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,29),(5,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,30),(6,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,31),(7,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,1),(8,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,33),(9,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,34),(10,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,35),(11,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,36),(12,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,37),(13,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,38),(14,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,3),(15,'Kohtenblatt',1,NULL,NULL,'0000-00-00 00:00:00',1,40),(16,'Jurtendach',1,NULL,NULL,'0000-00-00 00:00:00',3,41),(17,'Jurtendach',1,NULL,NULL,'0000-00-00 00:00:00',3,42),(18,'4m Mittelstange',1,NULL,'Tresdafdsf asdkfjasdijf asijgaisdfjglkfdsjgioreatporgns tgöe4ihgköjsnuh hjiousjkdsgio iofjgkldsf iisdhgiodsg ijfrsgiofhjdgiouhruh sg s isldpsi gsifd oisifosdj iodfsjgiof giosj iopos sdkijgio sdjosd gsig jsdfiojg is iij iojois jiosj iogj idskjgkldsf ','0000-00-00 00:00:00',13,43),(19,'4m Mittelstange',1,NULL,NULL,'0000-00-00 00:00:00',13,44),(20,'4m Mittelstange',1,NULL,NULL,'0000-00-00 00:00:00',13,45),(21,'4m Mittelstange',1,NULL,NULL,'0000-00-00 00:00:00',13,46),(22,'6m Seil',10,NULL,NULL,'0000-00-00 00:00:00',11,47),(26,'Abspannseil',50,NULL,NULL,'0000-00-00 00:00:00',9,52),(27,'Doppelzeltbahn',1,'4007295019010',NULL,'0000-00-00 00:00:00',5,53),(28,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,54),(29,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,55),(30,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,134),(31,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,61),(32,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,62),(33,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,63),(34,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,64),(35,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,4),(36,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,66),(37,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,67),(38,'Doppelzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',5,68),(39,'Halbdach',1,NULL,NULL,'0000-00-00 00:00:00',2,69),(40,'Halbdach',1,NULL,NULL,'0000-00-00 00:00:00',2,71),(41,'Hering',50,NULL,NULL,'0000-00-00 00:00:00',10,72),(42,'Jurtenabdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',6,73),(43,'Jurtenabdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',6,74),(44,'Jurtenabdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',6,75),(45,'Jurtendach',1,NULL,NULL,'0000-00-00 00:00:00',3,76),(46,'Jurtenspinne',1,NULL,NULL,'0000-00-00 00:00:00',7,77),(47,'Jurtenspinne',1,NULL,NULL,'0000-00-00 00:00:00',7,78),(48,'Jurtenspinne',1,NULL,NULL,'0000-00-00 00:00:00',7,79),(50,'KohtenAbdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',14,84),(51,'KohtenAbdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',14,85),(52,'KohtenAbdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',14,86),(53,'KohtenAbdeckplane',1,NULL,NULL,'0000-00-00 00:00:00',14,87),(54,'Kohtenkreuz',1,NULL,NULL,'0000-00-00 00:00:00',15,88),(55,'Kohtenkreuz',1,NULL,NULL,'0000-00-00 00:00:00',15,89),(56,'Kohtenkreuz',1,NULL,NULL,'0000-00-00 00:00:00',15,90),(57,'Kohtenkreuz',1,NULL,NULL,'0000-00-00 00:00:00',15,91),(58,'Steckstange',12,NULL,NULL,'0000-00-00 00:00:00',8,92),(59,'Steckstange',12,NULL,NULL,'0000-00-00 00:00:00',8,93),(60,'Steckstange',12,NULL,NULL,'0000-00-00 00:00:00',8,94),(61,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,95),(62,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,96),(63,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,97),(64,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,98),(65,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,99),(66,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,100),(67,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,101),(68,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,102),(69,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,103),(70,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,104),(71,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,105),(72,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,106),(73,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,107),(74,'\"Umlenk-/Blockseil-rolle',1,NULL,NULL,'0000-00-00 00:00:00',12,108),(75,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,109),(76,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,110),(77,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,111),(78,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,112),(79,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,113),(80,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,114),(81,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,115),(82,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,116),(83,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,117),(84,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,118),(85,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,119),(86,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,120),(87,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,121),(88,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,122),(89,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,123),(90,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,124),(91,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,125),(92,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,126),(93,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,127),(94,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,128),(95,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,129),(96,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,130),(97,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,131),(98,'Viereckzeltbahn',1,NULL,NULL,'0000-00-00 00:00:00',4,132);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_group`
--

DROP TABLE IF EXISTS `product_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_group`
--

LOCK TABLES `product_group` WRITE;
/*!40000 ALTER TABLE `product_group` DISABLE KEYS */;
INSERT INTO `product_group` VALUES (1,'Jurte','22.00'),(2,'Kohte','12.00'),(3,'Hoch Kothe','8.00');
/*!40000 ALTER TABLE `product_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_group_part`
--

DROP TABLE IF EXISTS `product_group_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_group_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F332749DFE54D947` (`group_id`),
  KEY `IDX_F332749D12469DE2` (`category_id`),
  CONSTRAINT `FK_F332749D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_F332749DFE54D947` FOREIGN KEY (`group_id`) REFERENCES `product_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_group_part`
--

LOCK TABLES `product_group_part` WRITE;
/*!40000 ALTER TABLE `product_group_part` DISABLE KEYS */;
INSERT INTO `product_group_part` VALUES (1,1,1,6),(2,1,4,12),(3,1,6,1),(4,1,7,1),(5,1,8,12),(6,1,9,12),(7,1,10,12),(8,1,11,2),(9,1,12,1),(10,1,13,1),(11,2,1,6),(12,2,14,1),(13,2,10,8),(14,2,11,2),(15,2,15,1),(16,2,13,1),(17,3,1,4),(18,3,14,1),(19,3,10,8),(20,3,8,8),(21,3,9,8),(22,3,11,2),(23,3,15,1),(24,3,13,1),(25,3,12,1);
/*!40000 ALTER TABLE `product_group_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserved_categorys`
--

DROP TABLE IF EXISTS `reserved_categorys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserved_categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B36AE5CBE00CEDDE` (`booking`),
  KEY `IDX_B36AE5CB64C19C1` (`category`),
  CONSTRAINT `FK_B36AE5CB64C19C1` FOREIGN KEY (`category`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_B36AE5CBE00CEDDE` FOREIGN KEY (`booking`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserved_categorys`
--

LOCK TABLES `reserved_categorys` WRITE;
/*!40000 ALTER TABLE `reserved_categorys` DISABLE KEYS */;
INSERT INTO `reserved_categorys` VALUES (1,1,1,12,'2017-01-31 21:21:50'),(2,1,4,12,'2017-01-31 21:21:50'),(3,1,6,1,'2017-01-31 21:21:50'),(4,1,7,1,'2017-01-31 21:21:50'),(5,1,8,12,'2017-01-31 21:21:50'),(6,1,9,12,'2017-01-31 21:21:50'),(7,1,10,20,'2017-01-31 21:21:50'),(8,1,11,4,'2017-01-31 21:21:50'),(9,1,12,1,'2017-01-31 21:21:50'),(10,1,13,2,'2017-01-31 21:21:50'),(11,1,14,1,'2017-01-31 21:21:50'),(12,1,15,1,'2017-01-31 21:21:50'),(13,1,2,1,'2017-01-31 21:21:50'),(14,1,3,1,'2017-01-31 21:21:50'),(15,2,1,4,'2017-01-31 21:29:45'),(16,2,14,1,'2017-01-31 21:29:45'),(17,2,10,8,'2017-01-31 21:29:45'),(18,2,8,9,'2017-01-31 21:29:45'),(19,2,9,8,'2017-01-31 21:29:45'),(20,2,11,2,'2017-01-31 21:29:45'),(21,2,15,1,'2017-01-31 21:29:45'),(22,2,13,1,'2017-01-31 21:29:45'),(23,2,12,1,'2017-01-31 21:29:45'),(24,2,3,1,'2017-01-31 21:29:45'),(25,2,4,1,'2017-01-31 21:29:45'),(26,2,5,1,'2017-01-31 21:29:45'),(27,2,7,1,'2017-01-31 21:29:45');
/*!40000 ALTER TABLE `reserved_categorys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_426EF392BF396750` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1),(3),(4);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prio` enum('success','info','warning','danger') COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:prio)',
  `default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Anfrage','success',0),(2,'Zur abholung bereit','success',0),(3,'Material erfolgreich zurückgenommen','success',0),(4,'Material mit Materialschäden zurückgenommen','warning',0),(5,'Rechnung wurde verschickt','success',0),(6,'Rechnung wurde bezahlt','success',0),(7,'Defekt muss repariert werden','danger',0),(8,'Zum Teil Beschädigt','warning',0),(9,'Sonstiges','info',0),(10,'Test','warning',0),(11,'Erstellt','success',1);
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_history`
--

DROP TABLE IF EXISTS `state_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking` int(11) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_61DA0AEDE00CEDDE` (`booking`),
  KEY `IDX_61DA0AEDD34A04AD` (`product`),
  KEY `IDX_61DA0AEDA393D2FB` (`state`),
  KEY `IDX_61DA0AEDA76ED395` (`user_id`),
  CONSTRAINT `FK_61DA0AEDA393D2FB` FOREIGN KEY (`state`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_61DA0AEDA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_61DA0AEDD34A04AD` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_61DA0AEDE00CEDDE` FOREIGN KEY (`booking`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_history`
--

LOCK TABLES `state_history` WRITE;
/*!40000 ALTER TABLE `state_history` DISABLE KEYS */;
INSERT INTO `state_history` VALUES (1,NULL,7,8,1,'2017-01-30 00:00:00','Die linke Öse lebt nicht mehr lange. Schon halb raus gerissen'),(2,NULL,14,8,1,'2017-01-30 00:00:00','kleines Brandloch auf der rechten seite.'),(3,NULL,14,7,2,'2017-01-31 00:00:00','Das Brandloch ist größer geworden. Muss nun genäht werden.'),(4,NULL,35,9,1,'2017-01-31 00:00:00','Die Plane ist verschlammt. Müsste mal gebürstet werden.'),(7,NULL,1,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(27,NULL,2,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(28,NULL,3,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(29,NULL,4,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(30,NULL,5,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(31,NULL,6,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(32,NULL,7,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(33,NULL,8,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(34,NULL,9,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(35,NULL,10,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(36,NULL,11,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(37,NULL,12,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(38,NULL,13,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(39,NULL,14,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(40,NULL,15,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(41,NULL,16,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(42,NULL,17,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(43,NULL,18,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(44,NULL,19,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(45,NULL,20,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(46,NULL,21,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(47,NULL,22,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(52,NULL,26,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(53,NULL,27,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(54,NULL,28,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(55,NULL,29,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(56,NULL,20,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(57,NULL,21,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(58,NULL,22,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(61,NULL,31,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(62,NULL,32,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(63,NULL,33,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(64,NULL,34,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(65,NULL,35,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(66,NULL,36,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(67,NULL,37,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(68,NULL,38,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(69,NULL,39,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(71,NULL,40,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(72,NULL,41,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(73,NULL,42,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(74,NULL,43,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(75,NULL,44,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(76,NULL,45,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(77,NULL,46,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(78,NULL,47,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(79,NULL,48,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(84,NULL,50,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(85,NULL,51,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(86,NULL,52,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(87,NULL,53,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(88,NULL,54,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(89,NULL,55,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(90,NULL,56,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(91,NULL,57,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(92,NULL,58,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(93,NULL,59,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(94,NULL,60,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(95,NULL,61,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(96,NULL,62,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(97,NULL,63,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(98,NULL,64,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(99,NULL,65,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(100,NULL,66,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(101,NULL,67,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(102,NULL,68,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(103,NULL,69,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(104,NULL,70,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(105,NULL,71,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(106,NULL,72,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(107,NULL,73,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(108,NULL,74,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(109,NULL,75,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(110,NULL,76,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(111,NULL,77,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(112,NULL,78,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(113,NULL,79,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(114,NULL,70,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(115,NULL,81,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(116,NULL,82,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(117,NULL,83,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(118,NULL,84,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(119,NULL,85,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(120,NULL,86,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(121,NULL,87,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(122,NULL,88,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(123,NULL,89,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(124,NULL,90,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(125,NULL,91,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(126,NULL,92,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(127,NULL,93,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(128,NULL,94,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(129,NULL,95,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(130,NULL,96,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(131,NULL,97,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(132,NULL,98,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(134,NULL,30,11,1,'2017-01-29 00:00:00','Produkt eingekauft'),(135,1,NULL,11,5,'2017-01-31 21:21:50',NULL),(136,1,NULL,2,5,'2017-01-31 21:22:10','Bereit'),(137,1,NULL,3,5,'2017-01-31 21:22:43','alles gut'),(138,2,NULL,11,5,'2017-01-31 21:29:45',NULL),(139,2,NULL,2,5,'2017-01-31 21:30:09','Abholen'),(140,2,NULL,2,5,'2017-01-31 21:30:30','abholen biotte');
/*!40000 ALTER TABLE `state_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'rolf@olf.de','rolf@olf.de','rolf@olf.de','rolf@olf.de',0,NULL,'$2y$13$tOJpOFWtRclp3xzjqnL63uR3NpeBtCQz0uq5SQRtwlgWJ1C/S9We.',NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}','staff'),(2,'sdfjdkf@dsd.de','sdfjdkf@dsd.de','sdfjdkf@dsd.de','sdfjdkf@dsd.de',0,NULL,'$2y$13$7gtVjxYJbwncJTF/ZUBreOcEKJIiHgOqydQ9nDKqHeCrAGeuSoln2',NULL,NULL,NULL,'a:1:{i:0;s:13:\"ROLE_CUSTOMER\";}','customer'),(3,'dfgd@fsd.fr','dfgd@fsd.fr','dfgd@fsd.fr','dfgd@fsd.fr',0,NULL,'$2y$13$aLQudhOwBANkdiYT9h00R.p3N4oPjhupXzbXQ6hevf9P3J3iy1cYK',NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}','staff'),(4,'a@a.com','a@a.com','a@a.com','a@a.com',0,NULL,'$2y$13$k1D4g7bwWdt4fupmxkvTh.QbiQCPalDCLeX/Cj1nA5SUy5P5aapjC',NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}','staff'),(5,'b@b.com','b@b.com','b@b.com','b@b.com',0,NULL,'$2y$13$DwFotDcrnRcMz7l3lC33kOWHSgTwV.1NjLLgDo./wjapvKBMVqhzG',NULL,NULL,NULL,'a:1:{i:0;s:13:\"ROLE_CUSTOMER\";}','customer');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-31 22:30:48
